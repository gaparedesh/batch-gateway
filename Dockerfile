FROM openjdk:8-jdk-alpine
MAINTAINER bravo
COPY target/batch-gateway.jar batch-gateway.jar
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=dev", "/batch-gateway.jar"]