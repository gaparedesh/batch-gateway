#!groovy

import groovy.json.JsonOutput

// Fields definition
String author
String message
String environment = 'Development'
String appName = 'batch-gateway'
String slackNotificationChannel = 'efact'

pipeline {
    agent any
    tools {
        maven 'Maven-3'
    }
    stages {
        stage("Checkout SCM") {
            steps {
                checkout scm
            }
        }
        stage("Environment Setup") {
            steps {
                sh "mvn --version"
                sh "java -version"
                script {
                    // Sets global variables
                    def commit = sh(returnStdout: true, script: 'git rev-parse HEAD')
                    author = sh(returnStdout: true, script: "git --no-pager show -s --format='%an' ${commit}").trim()
                    message = sh(returnStdout: true, script: 'git log -1 --pretty=%B').trim()
                    echo "Variables successfully set"
                }
            }
        }
        stage("Check Code Health") {
            when {
                not {
                    anyOf {
                        branch 'master'
                        branch 'develop'
                    }
                }
            }
            steps {
                configFileProvider([configFile(fileId: '552ca99b-1f32-4886-af74-fb0cdd994062', variable: 'MAVEN_SETTINGS_XML')]) {
                    sh "mvn clean install -Dfilter=false -U --batch-mode -s $MAVEN_SETTINGS_XML"
                }
            }
        }
        stage("Run Test Cases") {
            when {
                branch 'develop';
            }
            steps {
                configFileProvider([configFile(fileId: '552ca99b-1f32-4886-af74-fb0cdd994062', variable: 'MAVEN_SETTINGS_XML')]) {
                    sh "mvn clean test -Dfilter=false -U --batch-mode -s $MAVEN_SETTINGS_XML"
                }
            }
        }
        stage("Build Artifact") {
            steps {
                configFileProvider([configFile(fileId: '552ca99b-1f32-4886-af74-fb0cdd994062', variable: 'MAVEN_SETTINGS_XML')]) {
                    sh "mvn clean install -DskipTests -Dfilter=false -U --batch-mode -s $MAVEN_SETTINGS_XML"
                }
            }
        }
        stage("Deploy to Development") {
            when {
               branch 'develop';
            }
            steps {
                script {
                    doDeployment('Development', currentBuild.number, env.WORKSPACE as String)
                }
            }
        }
        stage("Deploy to Staging") {
            steps {
                script {
                    input(message: "Deploy to staging?")
                    doDeployment('Staging', currentBuild.number, env.WORKSPACE as String)
                }
            }
        }
        stage("Deploy to Production") {
            when {
               branch 'master';
            }
            steps {
                script {
                    input(message: "Continue with Production?")
                    doDeployment('Production', currentBuild.number, env.WORKSPACE as String)
                }
            }
        }
    }
    post {
        // When build succeed
        success {
            script {
                String result = "${currentBuild.result == null ? 'SUCCESS' : currentBuild.result}"
                notifySlack(result + '!',
                        slackNotificationChannel,
                        buildMessage(
                                author,
                                message,
                                env.JOB_NAME as String,
                                env.BUILD_NUMBER as Integer,
                                env.BRANCH_NAME as String,
                                env.BUILD_URL as String,
                                result
                        ))
            }
        }
        // When build failed
        failure {
            script {
                String result = "Failure"
                notifySlack(result + "!",
                        slackNotificationChannel,
                        buildMessage(
                                author,
                                message,
                                env.JOB_NAME as String,
                                env.BUILD_NUMBER as Integer,
                                env.BRANCH_NAME as String,
                                env.BUILD_URL as String,
                                result))
            }
        }
    }
}

def doDeployment(String environment, buildNumber, String envWorkspace) {
    final String appName = 'batch-gateway'
    // Read environments where it should deploy the artifact
    def environments = readJSON file: "${envWorkspace}/Environments.json"
    // Defines local variables
    def remotes = environments[environment].servers
    def profile = environments[environment].profile
    final String workspace = environments[environment].workspace ?: '.'

    // Defines commands to be run on remote servers
    String stopProcess = "systemctl stop appefact-batch-gateway"
    String versionBackup = "cd ${workspace} && cp -p ${appName}.jar ${appName}/${appName}-${buildNumber}.jar"
    String startService = "systemctl start appefact-batch-gateway"

    // Establishes remote connection and runs remote commands for each node
    for (remote in remotes) {
        // Retrieves the UsernamePassword stored in jenkins credentials manager
        withCredentials([usernamePassword(
                credentialsId: remote.credentials,
                usernameVariable: 'username',
                passwordVariable: 'password')]) {
            // sets user and password for ssh connection
            remote.user = username
            remote.password = password
            remote.pty = true

            // Backup the currently running version
            sshCommand remote: remote, command: versionBackup, failOnError: false
            // Upload the new artifact
            sshPut remote: remote, from: "target/${appName}.jar", into: workspace
            // Stops the running version on this remote
            sshCommand remote: remote, command: stopProcess, failOnError: false, sudo: true
            if (remote.serverNum == 1) {
                // Starts the service pointing to the current environment
                sshCommand remote: remote, command: startService, failOnError: true, sudo: true
            }
        }
    }
}

static def buildMessage(String author,
                        String message,
                        String job,
                        Integer build,
                        String branch,
                        String url,
                        String result) {
    return [
            [
                    "blocks": [
                            [
                                    "type": "header",
                                    "text": [
                                            "type" : "plain_text",
                                            "text" : "${job}, build #${build}",
                                            "emoji": true
                                    ]
                            ],
                            [
                                    "type"  : "section",
                                    "fields": [
                                            [
                                                    "type": "mrkdwn",
                                                    "text": "*Branch:*\n${branch}"
                                            ],
                                            [
                                                    "type": "mrkdwn",
                                                    "text": "*Build Number:*\n<${url}|${build}>"
                                            ]
                                    ]
                            ],
                            [
                                    "type"  : "section",
                                    "fields": [
                                            [
                                                    "type": "mrkdwn",
                                                    "text": "*Created by:*\n${author}"
                                            ],
                                            [
                                                    "type": "mrkdwn",
                                                    "text": "*When:*\nJust now"
                                            ]
                                    ]
                            ],
                            [
                                    "type"  : "section",
                                    "fields": [
                                            [
                                                    "type": "mrkdwn",
                                                    "text": "*Message:*\n${message}"
                                            ],
                                            [
                                                    "type": "mrkdwn",
                                                    "text": "*Result:*\n${result.toUpperCase()}"
                                            ]
                                    ]
                            ],
                            [
                                    "type": "section",
                                    "text": [
                                            "type": "mrkdwn",
                                            "text": "<${url}console|View logs>"
                                    ]
                            ]
                    ]
            ]
    ]
}

def notifySlack(text, channel, attachments) {
    String slackURL = 'https://hooks.slack.com/services/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'
    String jenkinsIcon = 'https://s3.amazonaws.com/bravo.public/jenkins-1200.png'

    def payload = JsonOutput.toJson([text       : text,
                                     channel    : channel,
                                     username   : "Jenkins",
                                     icon_url   : jenkinsIcon,
                                     attachments: attachments
    ])
    sh "curl -X POST --data-urlencode \'payload=${payload}\' ${slackURL}"
}