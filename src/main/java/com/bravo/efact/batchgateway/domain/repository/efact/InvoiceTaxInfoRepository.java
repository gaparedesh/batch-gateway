package com.bravo.efact.batchgateway.domain.repository.efact;

import com.bravo.efact.lib.domain.entity.InvoiceTaxInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceTaxInfoRepository extends JpaRepository<InvoiceTaxInfo, Long> {

}