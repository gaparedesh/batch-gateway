package com.bravo.efact.batchgateway.domain.entity.nopos;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Data
@Entity
@IdClass(value = NoPosInvoiceRefPK.class)
@Table(name = "SB_V_INFO_REFERENCIA_ECF")
public class NoPosInvoiceReference {

    @Id
    @Column(name = "ENCF")
    private String encf;
    @Id
    @Column(name = "NCF_MODIFICADO")
    private String encfModified;
    @Column(name = "CODIGO_MODIFICACION")
    private String modificationCode;
    @Column(name = "RAZON_MODIFICACION")
    private String reason;
    @Column(name = "TIPO_NC")
    private String modifiedType;
}
