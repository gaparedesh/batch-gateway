package com.bravo.efact.batchgateway.domain.entity.pos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@ToString
@IdClass(PosInvoiceDetailPk.class)
@Table(name = "sb_v_detalleFacturas", schema = "dbo")
public class PosInvoiceDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "idtrx", nullable = false)
    private Long id;

    @Id
    @Column(name = "subidtrx", nullable = false)
    private Long subId;

    @Column(name = "NumeroLinea", nullable = false)
    private Integer lineNumber;

    @Column(name = "TipoCodigo", nullable = false)
    private String codeType;

    @Column(name = "CodigoItem")
    private String itemCode;

    @Column(name = "IndicadorFacturacion", nullable = false)
    private String taxCategory;

    @Column(name = "Retencion", nullable = false)
    private String retentionAgentInd;

    @Column(name = "NombreItem")
    private String itemName;

    @Column(name = "IndicadorBienoServicio", nullable = false)
    private Integer serviceIndicator;

    @Column(name = "DescripcionItem")
    private String itemDescription;

    @Column(name = "CantidadItem")
    private Double itemQuantity;

    @Column(name = "UnidadMedida", nullable = false)
    private String unitMeasure;

    @Column(name = "CantidadReferencia", nullable = false)
    private String referenceQuantity;

    @Column(name = "UnidadReferencia", nullable = false)
    private String referenceUnit;

    @Column(name = "TablaSubcantidad", nullable = false)
    private String tablaSubcantidad;

    @Column(name = "GradosAlcohol", nullable = false)
    private Integer alcohol;

    @Column(name = "PrecioUnitarioReferencia")
    private Double refUnitPrice;

    @Column(name = "FechaElaboracion", nullable = false)
    private String fechaElaboracion;

    @Column(name = "FechaVencimientoItem", nullable = false)
    private String fechaVencimientoItem;

    @Column(name = "Mineria", nullable = false)
    private String mineria;

    @Column(name = "PrecioUnitarioItem")
    private Double unitPriceItem;

    @Column(name = "DescuentoMonto")
    private Double  discountAmount;

    @Column(name = "TipoSubDescuento", nullable = false)
    private String tipoSubDescuento;

    @Column(name = "SubDescuentoPorcentajE", nullable = false)
    private Integer subDescuentoPorcentajE;

    @Column(name = "MontoSubDescuento")
    private Double montoSubDescuento;

    @Column(name = "Recargo", nullable = false)
    private String recargo;

    @Column(name = "TablaImpuestoAdicion", nullable = false)
    private String tablaImpuestoAdicion;

    @Column(name = "OtraMoneda", nullable = false)
    private String otraMoneda;

    @Column(name = "MontoItem")
    private Double itemAmount;

    @Column(name = "Departamento")
    private String departamento;

    @Column(name = "itbis", nullable = false)
    private Double itbis;

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
