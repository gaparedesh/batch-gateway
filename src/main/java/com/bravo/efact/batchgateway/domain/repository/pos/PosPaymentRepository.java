package com.bravo.efact.batchgateway.domain.repository.pos;

import com.bravo.efact.batchgateway.domain.entity.pos.PosPayment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PosPaymentRepository extends JpaRepository<PosPayment, Long> {
    List<PosPayment> findAllByIdEquals(Long id);
}