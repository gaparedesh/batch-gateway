package com.bravo.efact.batchgateway.domain.entity.pos;

import com.bravo.efact.lib.domain.model.CustomDateFormatter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@Data
@Entity
@Table(name = "sb_v_EncabezadoFactura", schema = "dbo")
public class PosInvoiceHeader implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "idtrx", nullable = false)
    private Long id;

    @Column(name = "Version", nullable = false)
    private String version;

    @Column(name = "TipoeCF", nullable = false)
    private String type;

    @Column(name = "eNCF")
    private String ecf;

    @Column(name = "modified_efc")
    private String modifiedEcf;

    @Column(name = "FechaVencimientoSecuencia", nullable = false)
    private String sequenceExpirationDate;

    @Column(name = "IndicadorNotaCredito", nullable = false)
    private Boolean creditNoteInd;

    @Column(name = "IndicadorEnvioDiferido", nullable = false)
    private Boolean deferredShippingInd;

    @Column(name = "IndicadorMontoGravado", nullable = false)
    private Boolean taxedAmountInd;

    @Column(name = "TipoIngresos", nullable = false)
    private String incomeType;

    @Column(name = "TipoPago", nullable = false)
    private String paymentCondition;

    @Column(name = "FechaLimitePago", nullable = false)
    private String paymentDeadline;

    @Column(name = "TerminoPago", nullable = false)
    private String paymentTerms;

    @Column(name = "FechaDesde", nullable = false)
    private String billingInitDate;

    @Column(name = "FechaHasta", nullable = false)
    private String billingEndDate;

    @Column(name = "TotalPaginas", nullable = false)
    private Integer totalPages;

    @Column(name = "RNCEmisor", nullable = false)
    private String emitterRnc;

    @Column(name = "RazonSocialEmisor", nullable = false)
    private String emitterBusinessName;

    @Column(name = "Sucursal")
    private String sucursal;

    @Column(name = "DireccionEmisor")
    private String emitterAddress;

    @Column(name = "Municipio", nullable = false)
    private String town;

    @Column(name = "Provincia", nullable = false)
    private String province;

    @Column(name = "TelefonoEmisor", nullable = false)
    private String emitterPhone;

    @Column(name = "CorreoEmisor", nullable = false)
    private String emitterMail;

    @Column(name = "WebSite", nullable = false)
    private String webSite;

    @Column(name = "ActividadEconomica", nullable = false)
    private String activity;

    @Column(name = "CodigoVendedor")
    private String sellerCode;

    @Column(name = "NumeroFacturaInterna", nullable = false)
    private Long internalInvoiceNumber;

    @Column(name = "NumeroPedidoInterno", nullable = false)
    private String internalOrderNumber;

    @Column(name = "ZonaVenta", nullable = false)
    private String saleZone;

    @Column(name = "RutaVenta", nullable = false)
    private String saleRoute;

    @Column(name = "FechaEmision")
    private Date issueDate;

    @Column(name = "RNCComprador")
    private String buyerRnc;

    @Column(name = "IdentificadorExtranjero", nullable = false)
    private String foreignDni;

    @Column(name = "RazonSocialComprador", nullable = false)
    private String buyerBusinessName;

    @Column(name = "ContactoComprador", nullable = false)
    private String buyerContactInfo;

    @Column(name = "CorreoComprador", nullable = false)
    private String buyerEmail;

    @Column(name = "DireccionComprador", nullable = false)
    private String buyerAddress;

    @Column(name = "MunicipioComprador", nullable = false)
    private String buyerTown;

    @Column(name = "ProvinciaComprador", nullable = false)
    private String buyerProvince;

    @Column(name = "PaisComprador", nullable = false)
    private String buyerCountry;

    @Column(name = "FechaEntrega")
    private Date deliveryDate;

    @Column(name = "ContactoEntrega", nullable = false)
    private String deliveryContact;

    @Column(name = "DireccionEntrega", nullable = false)
    private String deliveryAddress;

    @Column(name = "TelefonoAdicional", nullable = false)
    private String additionalPhone;

    @Column(name = "FechaOrdenCompra")
    private Date purchaseOrderDate;

    @Column(name = "NumeroOrdenCompra", nullable = false)
    private String purchaseOrderNumber;

    @Column(name = "CodigoInternoComprador", nullable = false)
    private String buyerInternalCode;

    @Column(name = "ResponsablePago", nullable = false)
    private String paymentResponsible;

    @Column(name = "Informacionadicionalcomprador", nullable = false)
    private String buyerAdditionalInfo;

    @Column(name = "InformacionesAdicionales", nullable = false)
    private String additionalInfo;

    @Column(name = "MontoGravadoTotal")
    private Double totalTaxedAmount;

    @Column(name = "MontoGravadoI1")
    private Integer totalItbis1;

    @Column(name = "MontoGravadoI2")
    private Integer totalItbis2;

    @Column(name = "MontoGravadoI3")
    private Integer totalItbis3;

    @Column(name = "MontoExento")
    private Double totalExemptAmount;

    @Column(name = "ITBIS1")
    private Integer itbisRate1;

    @Column(name = "ITBIS2")
    private Integer itbisRate2;

    @Column(name = "ITBIS3", nullable = false)
    private Integer itbisRate3;

    @Column(name = "TotalITBIS")
    private Double totalItbisAmount;

    @Column(name = "TotalITBIS1")
    private Integer totalItbisRate1;

    @Column(name = "TotalITBIS2")
    private Integer totalItbisRate2;

    @Column(name = "TotalITBIS3", nullable = false)
    private Integer totalItbisRate3;

    @Column(name = "MontoImpuestoAdicional", nullable = false)
    private Double totalAdditionalTaxes;

    @Column(name = "ImpuestosAdicionales", nullable = false)
    private Integer additionalTaxType;

    @Column(name = "TipoImpuesto", nullable = false)
    private String additionalTaxTypeCode;

    @Column(name = "MontoImpuestoSelectivoConsumoEspecifico", nullable = false)
    private String iscSpecificAmount;

    @Column(name = "MontoImpuestoSelectivoConsumoAdvalorem", nullable = false)
    private String iscAdvaloremAmount;

    @Column(name = "OtrosImpuestosAdicionales", nullable = false)
    private String otherTaxesAmount;

    @Column(name = "MontoTotal")
    private Double invoiceTotalAmount;

    @Column(name = "MontoNoFacturable", nullable = false)
    private Double notBillableAmount;

    @Column(name = "MontoPeriodo")
    private Double periodAmount;

    @Column(name = "SaldoAnterior", nullable = false)
    private Integer previousBalance;

    @Column(name = "MontoAvancePago", nullable = false)
    private Integer paymentAdvanceAmount;

    @Column(name = "ValorPagar")
    private Double totalDueAmount;

    @Column(name = "VaTotalITBISRetenido", nullable = false)
    private Integer totalItbisRetention;

    @Column(name = "TotalISRRetencion", nullable = false)
    private Integer totalIsrRetention;

    @Column(name = "TotalITBISPercepcion", nullable = false)
    private Integer totalItbisPersived;

    @Column(name = "TotalISRPercepcion", nullable = false)
    private Integer totalIsrPersived;

    @JsonIgnore
    public Date getSequenceExpirationDate() {
        try {
            return CustomDateFormatter.POST_DATE_DASHED_FORMATTER.parse(sequenceExpirationDate);
        } catch (Exception e) {
            return null;
        }
    }

    @JsonIgnore
    public Date getPaymentDeadline() {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(this.issueDate);
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 1);
        calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        calendar.set(Calendar.DAY_OF_MONTH, 31);
        return calendar.getTime();
    }

    public Double getTotalExemptAmount() {
        return Objects.isNull(totalExemptAmount) ? 0 : totalExemptAmount;
    }

    public Double getTotalTaxedAmount() {
        return Objects.isNull(totalTaxedAmount) ? 0 : totalTaxedAmount;
    }

}
