package com.bravo.efact.batchgateway.domain.repository.efact;

import com.bravo.efact.lib.domain.entity.InvoiceItemAdjustment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceItemAdjustmentRepository extends JpaRepository<InvoiceItemAdjustment, Long> {
}
