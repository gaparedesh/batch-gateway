package com.bravo.efact.batchgateway.domain.entity.pos;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "sb_v_formadepago", schema = "dbo")
public class PosPayment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "idtrx", nullable = false)
    private Long id;

    @Column(name = "mdp")
    private String method;

    @Column(name = "FormaPago")
    private String paymentType;

    @Column(name = "monto")
    private Double amount;

    @Column(name = "TipoCuentaPago", nullable = false)
    private String accountType;

    @Column(name = "NumeroCuentaPago", nullable = false)
    private String accountNumber;

    @Column(name = "BancoPago", nullable = false)
    private String paymentBankName;

}
