package com.bravo.efact.batchgateway.domain.entity.pos;

import lombok.Data;

import java.io.Serializable;

@Data
public class PosInvoiceDetailPk implements Serializable {

    private Long id;
    private Long subId;
}
