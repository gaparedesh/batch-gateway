package com.bravo.efact.batchgateway.domain.repository.efact;

import com.bravo.efact.lib.domain.entity.InvoiceAdjustment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceAdjustmentRepository extends JpaRepository<InvoiceAdjustment, Long> {
}
