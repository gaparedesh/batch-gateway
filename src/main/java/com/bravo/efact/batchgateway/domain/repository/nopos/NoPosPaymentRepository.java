package com.bravo.efact.batchgateway.domain.repository.nopos;

import com.bravo.efact.batchgateway.domain.entity.nopos.NoPosPayment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NoPosPaymentRepository extends JpaRepository<NoPosPayment, String> {
    List<NoPosPayment> findAllByEcfEquals(String ecf);
}