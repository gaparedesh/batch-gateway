package com.bravo.efact.batchgateway.domain.repository.nopos;

import com.bravo.efact.batchgateway.domain.entity.nopos.NoPosInvoiceDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NoPosInvoiceDetailRepository extends JpaRepository<NoPosInvoiceDetail, String> {
    List<NoPosInvoiceDetail> findAllByEcfEquals(String ecf);
}