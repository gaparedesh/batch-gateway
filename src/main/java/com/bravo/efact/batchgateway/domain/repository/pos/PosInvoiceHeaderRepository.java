package com.bravo.efact.batchgateway.domain.repository.pos;

import com.bravo.efact.batchgateway.domain.entity.pos.PosInvoiceHeader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface PosInvoiceHeaderRepository extends JpaRepository<PosInvoiceHeader, Long>, JpaSpecificationExecutor<PosInvoiceHeader> {
    Page<PosInvoiceHeader> findAllByIdGreaterThanOrderByIdAsc(Long id, Pageable page);

    List<PosInvoiceHeader> findAllByOrderById();
}