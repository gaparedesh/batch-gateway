package com.bravo.efact.batchgateway.domain.repository.efact;

import com.bravo.efact.lib.domain.entity.InvoiceLoadError;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceLoadErrorRepository extends JpaRepository<InvoiceLoadError, Long> {
    Boolean existsByEncf(String encf);
}
