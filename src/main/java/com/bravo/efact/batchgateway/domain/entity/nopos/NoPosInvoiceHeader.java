package com.bravo.efact.batchgateway.domain.entity.nopos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "SB_V_FORMATO_VENTAS_ECF")
public class NoPosInvoiceHeader {

    @Id
    @Column(name = "IDSECUENCIAL")
    private Long id;

    @Column(name = "ENCF")
    private String ecf;

    @Column(name = "VERSION_FORMATO")
    private String version;

    @Column(name = "TIPO_CF")
    private String type;

    @Column(name = "FECHAVENCIMIENTOSECUENCIA")
    private String sequenceExpirationDate;

    @Column(name = "INDICADORENVIODIFERIDO")
    private Boolean deferredShippingInd;

    @Column(name = "INDICADORNOTACREDITO")
    private Boolean creditNoteInd;

    @Column(name = "INDICADORMONTOGRAVADO")
    private Boolean taxedAmountInd;

    @Column(name = "TIPODEINGRESOS")
    private String incomeType;

    @Column(name = "TIPOPAGO")
    private String paymentCondition;

    @Column(name = "FECHALIMITEPAGO")
    private String paymentDeadline;

    @Column(name = "TERMINOPAGO")
    private String paymentTerms;

    @Column(name = "TIPOCUENTADEPAGO")
    private String paymentAccountType;

    @Column(name = "CUENTADEPAGO")
    private String paymentAccountNum;

    @Column(name = "BANCODEPAGO")
    private String paymentBankName;

    @Column(name = "FECHADESDE")
    private String initBillingPeriod;

    @Column(name = "FECHAHASTA")
    private String endBillingPeriod;

    @Column(name = "TOTALPAGINAS")
    private Long totalPages;

    @Column(name = "RNCEMISOR")
    private String emitterRnc;

    @Column(name = "RAZONSOCIALEMISOR")
    private String emitterBusinessName;

    @Column(name = "NOMBRECOMERCIAL")
    private String emitterCommercialName;

    @Column(name = "SUCURSAL")
    private String emitterBranchName;

    @Column(name = "DIRECCIONEMISOR")
    private String emitterAddress;

    @Column(name = "MUNICIPIO")
    private String cityCode;

    @Column(name = "PROVINCIA")
    private String provinceCode;

    @Column(name = "CORREOEMISOR")
    private String emitterEmail;

    @Column(name = "WEBSITE")
    private String emitterWebsite;

    @Column(name = "ACTIVIDADECONOMICA")
    private String emitterBusinessActivity;

    @Column(name = "CODIGOVENDEDOR")
    private String vendorCode;

    @Column(name = "NUMEROFACTURAINTERNA")
    private String internalId;

    @Column(name = "NUMEROPEDIDOINTERNO")
    private String internalOrdeNum;

    @Column(name = "ZONAVENTA")
    private String vendorZone;

    @Column(name = "RUTAVENTA")
    private String vendorRoute;

    @Column(name = "INFORMACIONADICIONALEMISOR")
    private String emitterAdditionalInfo;

    @Column(name = "FECHAEMISION", nullable = false)
    private Date emissionDate;

    @Column(name = "RNCCOMPRADOR")
    private String buyerRnc;

    @Column(name = "INDICADOREXTRANJERO")
    private String foreingDni;

    @Column(name = "RAZONSOCIALCOMPRADOR")
    private String buyerBusinessName;

    @Column(name = "CONTACTOCOMPRADOR")
    private String buyerContactInfo;

    @Column(name = "CORREOCOMPRADOR")
    private String buyerEmail;

    @Column(name = "DIRECCIONCOMPRADOR")
    private String buyerAddress;

    @Column(name = "MUNICIPIOCOMPRADOR")
    private String buyerCityCode;

    @Column(name = "PROVINCIACOMPRADOR")
    private String buyerProvinceCode;

    @Column(name = "FECHAENTREGA")
    private String deliveryDate;

    @Column(name = "CONTACTOENTREGA")
    private String deliveryContact;

    @Column(name = "DIRECCIONENTREGA")
    private String deliveryAddress;

    @Column(name = "TELEFONOADICIONAL")
    private String deliveryContactPhone;

    @Column(name = "FECHAORDENCOMPRA")
    private String purchaseOrderDate;

    @Column(name = "NUMEROORDENCOMPRA")
    private String orderNum;

    @Column(name = "CODIGOINTERNOCOMPRADOR")
    private String buyerInternalCode;

    @Column(name = "RESPONSABLEPAGO")
    private String paymentResponsible;

    @Column(name = "INFORMACIONADICIONALCOMPRADOR")
    private String buyerAdditionalInfo;

    @Column(name = "MONTOGRAVADOTOTAL")
    private Double totalTaxedAmount;

    @Column(name = "MONTOGRAVADO1")
    private Double totalItbis1;

    @Column(name = "MONTOGRAVADO2")
    private Double totalItbis2;

    @Column(name = "MONTOGRAVADO3")
    private Double totalItbis3;

    @Column(name = "MONTOEXENTO")
    private Double totalExemptAmount;

    @Column(name = "ITBIS1")
    private Double itbisRate1;

    @Column(name = "ITBIS2")
    private Double itbisRate2;

    @Column(name = "ITBIS3")
    private Double itbisRate3;

    @Column(name = "TOTALITBIS")
    private Double totalItbisAmount;

    @Column(name = "TOTALITBIS1")
    private Double totalItbisRate1;

    @Column(name = "TOTALITBIS2")
    private Double totalItbisRate2;

    @Column(name = "TOTALITBIS3")
    private Double totalItbisRate3;

    @Column(name = "MONTOIMPUESTOADICIONAL")
    private Double totalAdditionalTaxes;

    @Column(name = "MONTOTOTAL")
    private Double invoiceTotalAmount;

    @Column(name = "MONTONOFACTURABLE")
    private Double notBillableAmount;

    @Column(name = "MONTOPERIODO")
    private String periodAmount;

    @Column(name = "SALDOANTERIOR")
    private String previousBalance;

    @Column(name = "MONTOAVANCEPAGO")
    private String paymentAdvanceAmount;

    @Column(name = "VALORPAGAR")
    private Double totalDueAmount;

    @Column(name = "TOTALITBISRETENIDO")
    private Double totalItbisRetention;

    @Column(name = "TOTALISRRETENCION")
    private Double totalIsrRetention;

    @Column(name = "TOTALITBISPERCEPCION")
    private Double totalItbisPersived;

    @Column(name = "TOTALISRPERCEPCION")
    private Double totalIsrPersived;

    @Column(name = "TIPOMONEDA")
    private String currencyType;

    @Column(name = "TIPOCAMBIO")
    private String exchangeRate;

    @Column(name = "MONTOGRAVADOTOTALOTRAMONEDA")
    private Double totalTaxedAmountOther;

    @Column(name = "MONTOGRAVADO1OTRAMONEDA")
    private String totalItbisOther1;

    @Column(name = "MONTOGRAVADO2OTRAMONEDA")
    private String totalItbisOther2;

    @Column(name = "MONTOGRAVADO3OTRAMONEDA")
    private String totalItbisOther3;

    @Column(name = "MONTOEXENTOOTRAMONEDA")
    private Double totalExemptAmountOther;

    @Column(name = "TOTALITBISOTRAMONEDA")
    private Double totalItbisAmountOther;

    @Column(name = "TOTALITBIS1OTRAMONEDA")
    private String totalItbisRateOther1;

    @Column(name = "TOTALITBIS2OTRAMONEDA")
    private String totalItbisRateOther2;

    @Column(name = "TOTALITBIS3OTRAMONEDA")
    private String totalItbisRateOther3;

    @Column(name = "MONTOIMPADICOTRAMONEDA")
    private Double totalAdditionalTaxesOther;

    @Column(name = "MONTOTOTALOTRAMONEDA")
    private Double invoiceTotalAmountOther;

    @Column(name = "organizacion_comercial")
    private String CommercialOrganization;

    @Column(name = "NUMERO_FACTURA")
    private String libraInvoiceNumber;

    @Transient
    private String encfModified;

    public Boolean getCreditNoteInd() {
        return Objects.nonNull(creditNoteInd) && creditNoteInd;
    }

    public String getCurrencyType() {
        return Objects.isNull(currencyType) ? "DOP" : currencyType;
    }

    public Boolean getDeferredShippingInd() {
        return Objects.nonNull(this.deferredShippingInd) && this.deferredShippingInd;
    }

    public Double getTotalDueAmount() {
        return Objects.isNull(totalDueAmount) ? 0.0 : totalDueAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        NoPosInvoiceHeader that = (NoPosInvoiceHeader) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public Double getTotalIsrPersived() {
        return Objects.isNull(totalIsrPersived) ? 0 : totalIsrPersived;
    }

    public Double getTotalIsrRetention() {
        return Objects.isNull(totalIsrRetention) ? 0 : totalIsrRetention;
    }

    public Double getTotalAdditionalTaxes() {
        return Objects.isNull(totalAdditionalTaxes) ? 0 : totalAdditionalTaxes;
    }

    public Double getInvoiceTotalAmount() {
        return Objects.isNull(invoiceTotalAmount) ? 0 : invoiceTotalAmount;
    }

    public Double getTotalAdditionalTaxesOther() {
        return Objects.isNull(totalAdditionalTaxesOther) ? 0 : totalAdditionalTaxesOther;
    }

    public Double getInvoiceTotalAmountOther() {
        return Objects.isNull(invoiceTotalAmountOther) ? 0 : invoiceTotalAmountOther;
    }

    public Double getTotalItbisPersived() {
        return Objects.isNull(totalItbisPersived) ? 0 : totalItbisPersived;
    }

    public Double getTotalItbisRetention() {
        return Objects.isNull(totalItbisRetention) ? 0 : totalItbisRetention;
    }
}
