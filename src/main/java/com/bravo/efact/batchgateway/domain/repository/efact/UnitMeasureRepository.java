package com.bravo.efact.batchgateway.domain.repository.efact;

import com.bravo.efact.lib.domain.entity.UnitMeasure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnitMeasureRepository extends JpaRepository<UnitMeasure, Long> {
}