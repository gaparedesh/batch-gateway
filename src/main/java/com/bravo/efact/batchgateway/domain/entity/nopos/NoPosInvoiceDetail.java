package com.bravo.efact.batchgateway.domain.entity.nopos;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.util.Objects;

@Data
@Entity
@IdClass(NoPosInvoiceDetailPk.class)
@Table(name = "SB_V_DET_VENTAS_SERVIC_ECF")
public class NoPosInvoiceDetail {

    @Id
    @Column(name = "ENCF")
    private String ecf;

    @Id
    @Column(name = "NUMEROLINEA", nullable = false)
    private Integer lineNumber;

    @Column(name = "INDICADORDEFACTURACION")
    private String taxCategory;

    @Column(name = "NOMBREITEM")
    private String itemName;

    @Column(name = "INDICADORBIENSERVICIO")
    private Boolean serviceIndicator;

    @Column(name = "DESCRIPCIONITEM")
    private String itemDescription;

    @Column(name = "CANTIDADITEM")
    private Double itemQuantity;

    @Column(name = "UNIDADMEDIDA")
    private String unitMeasure;

    @Column(name = "CANTIDADREFERENCIA")
    private Double referenceQuantity;

    @Column(name = "UNIDADREFERENCIA")
    private String referenceUnit;

    @Column(name = "GRADOSDEALCOHOL")
    private Double alcohol;

    @Column(name = "PRECIOUNITARIOREFERENCIA")
    private Double refUnitPrice;

    @Column(name = "PRECIOUNITARIOITEM")
    private Double unitPriceItem;

    @Column(name = "MONTODESCUENTO")
    private Double discountAmount;

    @Column(name = "MONTORECARGO")
    private Double surcharge;

    @Column(name = "PRECIOTRAMONEDA")
    private Double unitPriceItemOther;

    @Column(name = "DESCUENTOOTRAMONEDA")
    private Double discountAmountOther;

    @Column(name = "RECARGOOTRAMONEDA")
    private Double surchargeOther;

    @Column(name = "MONTOITEMOTRAMONEDA")
    private Double itemAmountOther;

    @Column(name = "MONTOITEM")
    private Double itemAmount;

    @Column(name = "indagenteretencion")
    private String retentionAgentIndicator;

    @Column(name = "montoITBISRetenido")
    private Double retentionITBISAmount;

    @Column(name = "MontoISRRetenido")
    private Double retentionISRRetenido;

    public String getItemDescription() {
        return Objects.isNull(itemDescription) ? this.itemName : this.itemDescription;
    }

    public Boolean getServiceIndicator() {
        return Objects.nonNull(serviceIndicator) && serviceIndicator;
    }

    public String getUnitMeasure() {
        return Objects.isNull(unitMeasure) ? "43" : unitMeasure;
    }

    public String getRetentionAgentIndicator() {
        return Objects.isNull(retentionAgentIndicator) ? null : retentionAgentIndicator.equals("1") ? "R": "P";
    }

    public Double getRetentionITBISAmount() {
        return Objects.isNull(retentionITBISAmount)? 0 : retentionITBISAmount;
    }

    public Double getRetentionISRRetenido() {
        return Objects.isNull(retentionISRRetenido)? 0 : retentionISRRetenido;
    }
}
