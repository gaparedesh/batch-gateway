package com.bravo.efact.batchgateway.domain.repository.efact;

import com.bravo.efact.lib.domain.entity.InvoicePagination;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoicePaginationRepository extends JpaRepository<InvoicePagination, Long> {

}