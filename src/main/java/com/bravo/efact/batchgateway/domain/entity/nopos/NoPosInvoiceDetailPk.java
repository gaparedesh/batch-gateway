package com.bravo.efact.batchgateway.domain.entity.nopos;

import lombok.Data;

import java.io.Serializable;

@Data
public class NoPosInvoiceDetailPk implements Serializable {
    private String ecf;
    private Integer lineNumber;
}
