package com.bravo.efact.batchgateway.domain.repository.efact;

import com.bravo.efact.lib.domain.entity.InvoiceSubtotal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceSubtotalRepository extends JpaRepository<InvoiceSubtotal, Long> {

}