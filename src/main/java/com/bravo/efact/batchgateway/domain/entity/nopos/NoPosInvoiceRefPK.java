package com.bravo.efact.batchgateway.domain.entity.nopos;

import lombok.Data;

import java.io.Serializable;

@Data
public class NoPosInvoiceRefPK implements Serializable {

    private String encf;
    private String encfModified;

}
