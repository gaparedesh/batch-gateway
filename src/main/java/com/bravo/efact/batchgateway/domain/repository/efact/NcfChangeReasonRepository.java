package com.bravo.efact.batchgateway.domain.repository.efact;

import com.bravo.efact.lib.domain.entity.NcfChangeReason;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NcfChangeReasonRepository extends JpaRepository<NcfChangeReason, Long> {

}