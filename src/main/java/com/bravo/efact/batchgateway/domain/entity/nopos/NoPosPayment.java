package com.bravo.efact.batchgateway.domain.entity.nopos;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "SB_V_FORMA_PAGO_DGII")
public class NoPosPayment {

    @Id
    @Column(name = "ENCF")
    private String ecf;

    @Column(name = "FORMADEPAGO")
    private String paymentType;

    @Column(name = "MONTODEPAGO")
    private Double amount;

}
