package com.bravo.efact.batchgateway.domain.repository.pos;

import com.bravo.efact.batchgateway.domain.entity.pos.PosInvoiceDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PosInvoiceDetailRepository extends JpaRepository<PosInvoiceDetail, Long>, JpaSpecificationExecutor<PosInvoiceDetail> {
    List<PosInvoiceDetail> findAllByIdEquals(Long id);
}