package com.bravo.efact.batchgateway.domain.repository.efact;

import com.bravo.efact.lib.domain.entity.PaymentCondition;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentConditionRepository extends JpaRepository<PaymentCondition, Long> {

}