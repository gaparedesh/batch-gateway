package com.bravo.efact.batchgateway.domain.repository.nopos;

import com.bravo.efact.batchgateway.domain.entity.nopos.NoPosInvoiceHeader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NoPosInvoiceHeaderRepository extends JpaRepository<NoPosInvoiceHeader, Long> {
    Page<NoPosInvoiceHeader> findAllByIdGreaterThanOrderByIdAsc(Long id, Pageable page);

    List<NoPosInvoiceHeader> findAllByOrderById();
}