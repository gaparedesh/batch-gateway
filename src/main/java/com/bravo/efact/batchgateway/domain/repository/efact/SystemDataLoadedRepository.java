package com.bravo.efact.batchgateway.domain.repository.efact;

import com.bravo.efact.batchgateway.domain.entity.efact.SystemDataLoaded;
import com.bravo.efact.lib.domain.model.SystemSource;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SystemDataLoadedRepository extends JpaRepository<SystemDataLoaded, Long> {
    Optional<SystemDataLoaded> findTopBySystemSourceOrderByLastDocumentDesc(SystemSource source);
}