package com.bravo.efact.batchgateway.domain.entity.efact;

import com.bravo.efact.lib.domain.model.SystemSource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "system_data_loaded")
public class SystemDataLoaded implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "loaded_tmp")
    private Date loadedTmp;

    @Enumerated(EnumType.STRING)
    @Column(name = "system_source", nullable = false)
    private SystemSource systemSource;

    @Column(name = "first_document", nullable = false)
    private Long firstDocument;

    @Column(name = "last_document")
    private Long lastDocument;

    @Column(name = "total_loaded")
    private Integer totalLoaded;

}
