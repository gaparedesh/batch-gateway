package com.bravo.efact.batchgateway.domain.repository.nopos;

import com.bravo.efact.batchgateway.domain.entity.nopos.NoPosInvoiceReference;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface NoPosInvoiceRefRepository  extends JpaRepository<NoPosInvoiceReference, String> {
    Optional<NoPosInvoiceReference> findByEncf(String encf);
}
