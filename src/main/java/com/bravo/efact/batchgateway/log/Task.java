package com.bravo.efact.batchgateway.log;

import com.bravo.utils.domain.model.log.TransactionLog;
import com.bravo.utils.serializer.TransactionLogSerializer;
import com.bravo.utils.service.log.LogService;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.slf4j.MDC;

@JsonSerialize(using = TransactionLogSerializer.class)
public enum Task implements TransactionLog {
    PERSIST_POS_INVOICE("Guardar información de la factura de punto de venta"),
    PERSIST_NO_POS_INVOICE("Guardar información de la factura");

    private final String description;

    Task(String desc) {
        this.description = desc;
    }

    @Override
    public String getId() {
        return MDC.get(LogService.transaction_id);
    }

    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public String getDescription() {
        return description;
    }
}
