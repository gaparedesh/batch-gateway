package com.bravo.efact.batchgateway.job;

import com.bravo.efact.batchgateway.service.pos.PosService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PosInvoiceSchedule {

    private final PosService posService;

    public PosInvoiceSchedule(PosService posService) {
        this.posService = posService;
    }

    @Scheduled(fixedDelayString = "${cron.rate.fixed.milliseconds.pos}")
    public void reportPosInvoices() {
        log.info("Process started: POS invoice loader");
        posService.loadAndStoreData();
    }
}
