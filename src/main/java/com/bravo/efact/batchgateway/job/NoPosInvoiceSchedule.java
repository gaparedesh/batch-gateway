package com.bravo.efact.batchgateway.job;

import com.bravo.efact.batchgateway.service.log.InternalLogService;
import com.bravo.efact.batchgateway.service.nopos.NoPosService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NoPosInvoiceSchedule {

    private final NoPosService noPosService;

    public NoPosInvoiceSchedule(NoPosService noPosService) {
        this.noPosService = noPosService;
    }

    @Scheduled(fixedDelayString = "${cron.rate.fixed.milliseconds.no-pos}")
    public void reportNoPosInvoices() {
        InternalLogService.init();
        log.info("Process started: NO-POS invoice loader");
        noPosService.loadAndStoreData();
        InternalLogService.finish();
    }
}
