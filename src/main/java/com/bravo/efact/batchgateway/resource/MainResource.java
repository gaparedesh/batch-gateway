package com.bravo.efact.batchgateway.resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping(value = {"", "/"})
public class MainResource {

    private final String HEALTH_URL = "/service/app-info";

    @GetMapping
    public RedirectView redirect() {
        return new RedirectView(HEALTH_URL);
    }

    @ResponseBody
    @GetMapping(value = HEALTH_URL)
    public ResponseEntity<Map<String, String>> redirect(@Value("${project.name}") String name,
                                                        @Value("${project.version}") String version,
                                                        @Value("${spring.profiles.active:unknown}") String profile) {
        Map<String, String> info = new HashMap<>();
        info.put("version", version);
        info.put("name", name);
        info.put("environment", profile);
        return ResponseEntity.ok(info);

    }

    @GetMapping(value = "/**/{path:[^.]*}")
    public RedirectView redirect(@PathVariable String path) {
        log.warn(String.format("Redirecting from: %s", path));
        return new RedirectView(HEALTH_URL);
    }
}
