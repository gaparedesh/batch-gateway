package com.bravo.efact.batchgateway.util;

import lombok.experimental.UtilityClass;
import java.io.PrintWriter;
import java.io.StringWriter;


@UtilityClass

public class ExceptionUtils {

    public String exceptionToString(Exception ex) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);

        return sw.toString();

    }

}