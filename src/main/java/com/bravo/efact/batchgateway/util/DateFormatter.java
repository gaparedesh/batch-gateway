package com.bravo.efact.batchgateway.util;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@Slf4j
public enum DateFormatter {
    LIBRA("dd-MM-yyyy");
    private final SimpleDateFormat formatter;

    DateFormatter(String format) {
        this.formatter = new SimpleDateFormat(format);
    }

    public Date format(String value) {
        try {
            return formatter.parse(value);
        } catch (ParseException e) {
            log.warn("Invalid date format: {}", e.getMessage(), e);
            return null;
        }
    }

    public Date format(String value, String orElse) {
        try {
            return formatter.parse(value);
        } catch (Exception e) {
            final Date date = format(orElse);
            if (Objects.isNull(date)) {
                return null;
            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, 1);
            calendar.set(Calendar.DAY_OF_MONTH, 15);
            return calendar.getTime();
        }
    }

    public Date format(String value, Date orElse) {
        try {
            return formatter.parse(value);
        } catch (Exception e) {
            return orElse;
        }
    }
}
