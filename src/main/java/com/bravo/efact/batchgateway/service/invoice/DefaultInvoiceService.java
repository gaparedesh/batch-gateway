package com.bravo.efact.batchgateway.service.invoice;

import com.bravo.efact.batchgateway.domain.entity.nopos.NoPosInvoiceHeader;
import com.bravo.efact.batchgateway.domain.entity.pos.PosInvoiceDetail;
import com.bravo.efact.batchgateway.domain.entity.pos.PosInvoiceHeader;
import com.bravo.efact.batchgateway.domain.repository.efact.DocumentTypeRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.IncomeTypeRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.InvoiceAdjustmentRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.InvoiceItemAdjustmentRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.InvoiceItemRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.InvoiceLoadErrorRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.InvoicePaginationRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.InvoicePaymentRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.InvoiceRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.InvoiceStatusRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.InvoiceSubtotalRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.InvoiceTaxInfoRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.InvoiceTypeRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.NcfChangeReasonRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.PaymentConditionRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.TaxCategoryRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.UnitMeasureRepository;
import com.bravo.efact.batchgateway.domain.repository.pos.PosInvoiceDetailRepository;
import com.bravo.efact.batchgateway.log.Task;
import com.bravo.efact.batchgateway.service.log.InternalLogService;
import com.bravo.efact.lib.domain.entity.DocumentType;
import com.bravo.efact.lib.domain.entity.IncomeType;
import com.bravo.efact.lib.domain.entity.Invoice;
import com.bravo.efact.lib.domain.entity.InvoiceAdjustment;
import com.bravo.efact.lib.domain.entity.InvoiceItem;
import com.bravo.efact.lib.domain.entity.InvoiceItemAdjustment;
import com.bravo.efact.lib.domain.entity.InvoiceLoadError;
import com.bravo.efact.lib.domain.entity.InvoicePagination;
import com.bravo.efact.lib.domain.entity.InvoicePayment;
import com.bravo.efact.lib.domain.entity.InvoiceStatus;
import com.bravo.efact.lib.domain.entity.InvoiceSubtotal;
import com.bravo.efact.lib.domain.entity.InvoiceTaxInfo;
import com.bravo.efact.lib.domain.entity.InvoiceType;
import com.bravo.efact.lib.domain.entity.NcfChangeReason;
import com.bravo.efact.lib.domain.entity.PaymentCondition;
import com.bravo.efact.lib.domain.entity.TaxCategory;
import com.bravo.efact.lib.domain.entity.UnitMeasure;
import com.bravo.efact.lib.domain.model.AdjustmentType;
import com.bravo.efact.lib.domain.model.AdjustmentValueType;
import com.bravo.efact.lib.domain.model.SystemSource;
import com.bravo.efact.lib.utils.Numbers;
import com.bravo.utils.domain.model.mail.MailRequest;
import com.bravo.utils.service.mail.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DefaultInvoiceService implements InvoiceService {

    private static final String  INVOICE_ORIGIN = "invoiceOrigin";
    private static final String  NCF = "ncf";
    private static final String  EMISSION_RNC = "rncEmisor";
    private static final String  BUYER_RNC = "rncComprador";
    private static final String  EVENT_DATE = "eventDate";

    @Value("${mail.invoiceErrorLoad.subject}")
    private String subject;

    @Value("${mail.invoiceErrorLoad.templateId}")
    private String templateId;

    @Value("${mail.invoiceErrorLoad.category}")
    private String category;

    @Value("${mail.invoiceErrorLoad.to}")
    private String tops;

    private final InvoiceRepository invoiceRepository;
    private final InternalLogService internalLogService;
    private final UnitMeasureRepository measureRepository;
    private final IncomeTypeRepository incomeTypeRepository;
    private final InvoicePaymentRepository paymentRepository;
    private final InvoiceTaxInfoRepository taxInfoRepository;
    private final TaxCategoryRepository taxCategoryRepository;
    private final InvoiceItemRepository invoiceItemRepository;
    private final InvoiceTypeRepository invoiceTypeRepository;
    private final InvoiceSubtotalRepository subtotalRepository;
    private final DocumentTypeRepository documentTypeRepository;
    private final InvoiceStatusRepository invoiceStatusRepository;
    private final NcfChangeReasonRepository changeReasonRepository;
    private final InvoicePaginationRepository paginationRepository;
    private final PaymentConditionRepository paymentConditionRepository;
    private final InvoiceAdjustmentRepository invoiceAdjustmentRepository;
    private final PosInvoiceDetailRepository invoiceDetailRepository;
    private final InvoiceItemAdjustmentRepository invoiceItemAdjustmentRepository;
    private final InvoiceLoadErrorRepository invoiceLoadErrorRepository;
    private final MailService mailService;

    public DefaultInvoiceService(InvoiceTypeRepository invoiceTypeRepository,
                                 DocumentTypeRepository documentTypeRepository,
                                 InvoiceStatusRepository invoiceStatusRepository,
                                 IncomeTypeRepository incomeTypeRepository,
                                 NcfChangeReasonRepository changeReasonRepository,
                                 InvoiceRepository invoiceRepository,
                                 PaymentConditionRepository paymentConditionRepository,
                                 InvoiceItemRepository invoiceItemRepository,
                                 InvoiceTaxInfoRepository taxInfoRepository,
                                 UnitMeasureRepository measureRepository,
                                 TaxCategoryRepository taxCategoryRepository,
                                 InvoiceSubtotalRepository subtotalRepository,
                                 InvoicePaginationRepository paginationRepository, InvoicePaymentRepository paymentRepository, InternalLogService internalLogService, InvoiceAdjustmentRepository invoiceAdjustmentRepository, PosInvoiceDetailRepository invoiceDetailRepository, InvoiceItemAdjustmentRepository invoiceItemAdjustmentRepository, InvoiceLoadErrorRepository invoiceLoadErrorRepository, MailService mailService) {
        this.invoiceTypeRepository = invoiceTypeRepository;
        this.documentTypeRepository = documentTypeRepository;
        this.invoiceStatusRepository = invoiceStatusRepository;
        this.incomeTypeRepository = incomeTypeRepository;
        this.changeReasonRepository = changeReasonRepository;
        this.invoiceRepository = invoiceRepository;
        this.paymentConditionRepository = paymentConditionRepository;
        this.invoiceItemRepository = invoiceItemRepository;
        this.taxInfoRepository = taxInfoRepository;
        this.measureRepository = measureRepository;
        this.taxCategoryRepository = taxCategoryRepository;
        this.subtotalRepository = subtotalRepository;
        this.paginationRepository = paginationRepository;
        this.paymentRepository = paymentRepository;
        this.internalLogService = internalLogService;
        this.invoiceAdjustmentRepository = invoiceAdjustmentRepository;
        this.invoiceDetailRepository = invoiceDetailRepository;
        this.invoiceItemAdjustmentRepository = invoiceItemAdjustmentRepository;
        this.invoiceLoadErrorRepository = invoiceLoadErrorRepository;
        this.mailService = mailService;
    }

    @Override
    @Transactional
    public Invoice save(PosInvoiceHeader posInvoice, Invoice invoice, List<InvoiceItem> items, List<InvoicePayment> payments) {
        try {
            // SAVE INVOICE BEFORE ASSIGNING ALL OTHERS FIELDS
            this.buildInvoiceBasics(invoice, items, payments);
            // BUILD AND STORE TAX INFO
            this.buildTaxInfo(posInvoice, invoice);
            // BUILD AND STORE SUBTOTAL INFO
            this.buildSubtotal(posInvoice, invoice);
            // BUILD AND STORE DISCOUNT AND PROMO
            this.buildInvoiceAdjustment(posInvoice, invoice, items);
            // BUILD AND STORE PAGINATION INFO
            this.buildPagination(posInvoice, invoice, items);
            log.info(String.format(invoice_persisted, SystemSource.POS, invoice.getEncf()));
            return invoice;
        } catch (Exception error) {
            final String message = String.format(invoice_not_persisted, SystemSource.POS, invoice.getEncf());
            log.error(message, error);
            final Map<String, Object> params = new HashMap<>();
            params.put("posInvoice", posInvoice);
            params.put("invoice", invoice);
            params.put("items", items);
            params.put("payments", payments);
            params.put("source", SystemSource.POS);
            this.internalLogService.error(Task.PERSIST_POS_INVOICE, message, posInvoice.getEcf(), error, params);
            return null;
        }
    }

    @Override
    @Transactional
    public Invoice save(NoPosInvoiceHeader noPosInvoice, Invoice invoice, List<InvoiceItem> items, List<InvoicePayment> payments) {
        try {
            // SAVE INVOICE BEFORE ASSIGNING ALL OTHERS FIELDS
            this.buildInvoiceBasics(invoice, items, payments);
            // BUILD AND STORE SUBTOTAL INFO
            this.buildSubtotal(noPosInvoice, invoice);
            // BUILD AND STORE TAX INFO
            this.buildTaxInfo(noPosInvoice, invoice);
            // BUILD AND STORE PAGINATION INFO
            this.buildPagination(noPosInvoice, invoice, items);
            log.info(String.format(invoice_persisted, SystemSource.NO_POS, invoice.getEncf()));
            return invoice;
        } catch (Exception error) {
            final String message = String.format(invoice_not_persisted, SystemSource.NO_POS, invoice.getEncf());
            log.error(message, error);
            final Map<String, Object> params = new HashMap<>();
            params.put("noPosInvoice", noPosInvoice);
            params.put("invoice", invoice);
            params.put("items", items);
            params.put("payments", payments);
            params.put("source", SystemSource.NO_POS);
            this.internalLogService.error(Task.PERSIST_NO_POS_INVOICE, message, noPosInvoice.getEcf(), error, params);
            return null;
        }
    }

    @Override
    public List<InvoiceType> invoiceTypes() {
        return this.invoiceTypeRepository.findAll();
    }

    @Override
    @Transactional
    public DocumentType documentType() {
        return this.documentTypeRepository.findTopByName(document_type)
                .orElseGet(() -> {
                    final DocumentType type = new DocumentType().setName(document_type);
                    return this.documentTypeRepository.save(type);
                });
    }

    @Override
    public InvoiceStatus invoiceStatus() {
        return this.invoiceStatusRepository.findTopByName(invoice_status)
                .orElseGet(() -> {
                    final InvoiceStatus type = new InvoiceStatus().setName(invoice_status);
                    return this.invoiceStatusRepository.save(type);
                });
    }

    @Override
    public List<IncomeType> incomeTypes() {
        return this.incomeTypeRepository.findAll();
    }

    @Override
    public List<NcfChangeReason> changeReasons() {
        return this.changeReasonRepository.findAll();
    }

    @Override
    public List<PaymentCondition> paymentConditions() {
        return this.paymentConditionRepository.findAll();
    }

    @Override
    public List<UnitMeasure> unitMeasures() {
        return this.measureRepository.findAll();
    }

    @Override
    public List<TaxCategory> taxCategories() {
        return this.taxCategoryRepository.findAll();
    }

    @Override
    public void invoiceLoadError(InvoiceLoadError error, String systemSource) {
        if(!this.invoiceLoadErrorRepository.existsByEncf(error.getEncf())){
            this.invoiceLoadErrorRepository.save(error);
            this.sendEmail(error, systemSource);
        }
    }

    private void sendEmail(InvoiceLoadError invoiceError, String systemSource) {
        final Map<String, Object> params = new HashMap<>();

        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");

        params.put(INVOICE_ORIGIN, systemSource);
        params.put(NCF, invoiceError.getEncf());
        params.put(EMISSION_RNC, invoiceError.getEmitterRnc());
        params.put(BUYER_RNC, invoiceError.getBuyerRnc());
        params.put(EVENT_DATE, newFormat.format(new Date()));

        MailRequest mail = MailRequest.builder()
                .subject(subject)
                .templateId(templateId)
                .mailCategory(category)
                .parameters(params)
                .to(Arrays.asList(tops.split(",")))
                .build();

        mailService.sendMail(mail);
    }

    private void buildInvoiceAdjustment(PosInvoiceHeader posInvoice, Invoice invoice, List<InvoiceItem> items) {
        final List<PosInvoiceDetail> details = this.invoiceDetailRepository.findAllByIdEquals(posInvoice.getId());
        AtomicInteger count=new AtomicInteger(0);

        List<InvoiceAdjustment> invoiceAdjustments = details.stream()
                                                        .filter(i -> Objects.isNull(i.getDepartamento())  && i.getDiscountAmount() > 0)
                                                        .map(itm -> InvoiceAdjustment.builder()
                                                                .adjustmentAmount(itm.getDiscountAmount())
                                                                .lineNumber(count.incrementAndGet())
                                                                .description(itm.getItemDescription())
                                                                .adjustmentType(AdjustmentType.DISCOUNT)
                                                                .valueType(AdjustmentValueType.AMOUNT)
                                                                .billingTypeInd(Integer.parseInt(itm.getTaxCategory()))
                                                                .invoice(invoice)
                                                                .build()).collect(Collectors.toList());
        this.invoiceAdjustmentRepository.saveAll(invoiceAdjustments);

        List<InvoiceItemAdjustment> invoiceItemAdjustments = details.stream()
                                                        .filter(i -> Objects.nonNull(i.getDepartamento())  && i.getDiscountAmount() > 0)
                .map(itm -> InvoiceItemAdjustment.builder()
                        .adjustmentAmount(itm.getDiscountAmount())
                        .adjustmentType(AdjustmentType.DISCOUNT)
                        .valueType(AdjustmentValueType.AMOUNT)
                        .invoiceItem(items.stream().filter(it -> it.getItemDescription().equalsIgnoreCase(itm.getItemDescription())).findFirst().get())
                        .build()
                ).collect(Collectors.toList());
        this.invoiceItemAdjustmentRepository.saveAll(invoiceItemAdjustments);
    }

    private void buildTaxInfo(PosInvoiceHeader posInvoice, Invoice invoice) {
        // BUILD TAX INFO
        InvoiceTaxInfo taxInfo = InvoiceTaxInfo.builder()
                .itbisRate1(Numbers.centesimal(posInvoice.getItbisRate1()).intValue())
                .itbisRate2(Numbers.centesimal(posInvoice.getItbisRate2()).intValue())
                .itbisRate3(Numbers.centesimal(posInvoice.getItbisRate3()).intValue())
                .totalItbis1(Numbers.centesimal(posInvoice.getTotalItbis1()))
                .totalItbis2(Numbers.centesimal(posInvoice.getTotalItbis2()))
                .totalItbis3(Numbers.centesimal(posInvoice.getTotalItbis3()))
                .totalItbisRate1(Numbers.centesimal(posInvoice.getTotalItbisRate1()))
                .totalItbisRate2(Numbers.centesimal(posInvoice.getTotalItbisRate2()))
                .totalItbisRate3(Numbers.centesimal(posInvoice.getTotalItbisRate3()))
                .totalIsrRetention(Numbers.centesimal(posInvoice.getTotalIsrRetention()))
                .totalItbisRetention(Numbers.centesimal(posInvoice.getTotalItbisRetention()))
                .totalIsrPersived(Numbers.centesimal(posInvoice.getTotalIsrPersived()))
                .totalItbisPersived(Numbers.centesimal(posInvoice.getTotalItbisPersived()))
                .invoice(invoice)
                .build();
        // PERSISTS TAX INFO
        this.taxInfoRepository.save(taxInfo);
    }

    private void buildTaxInfo(NoPosInvoiceHeader posInvoice, Invoice invoice) {
        // BUILD TAX INFO
        InvoiceTaxInfo taxInfo = InvoiceTaxInfo.builder()
                .itbisRate1(posInvoice.getItbisRate1().intValue())
                .itbisRate2(posInvoice.getItbisRate2().intValue())
                .itbisRate3(posInvoice.getItbisRate3().intValue())
                .totalItbis1(posInvoice.getTotalItbis1())
                .totalItbis2(posInvoice.getTotalItbis2())
                .totalItbis3(posInvoice.getTotalItbis3())
                .totalItbisRate1(posInvoice.getTotalItbisRate1())
                .totalItbisRate2(posInvoice.getTotalItbisRate2())
                .totalItbisRate3(posInvoice.getTotalItbisRate3())
                .totalIsrRetention(posInvoice.getTotalIsrRetention())
                .totalItbisRetention(posInvoice.getTotalItbisRetention())
                .totalIsrPersived(posInvoice.getTotalIsrPersived())
                .totalItbisPersived(posInvoice.getTotalItbisPersived())
                .invoice(invoice)
                .build();
        // PERSISTS TAX INFO
        this.taxInfoRepository.save(taxInfo);
    }

    private void buildSubtotal(PosInvoiceHeader posInvoice, Invoice invoice) {
        // BUILD SUBTOTAL OBJECT
        final InvoiceSubtotal subtotal = InvoiceSubtotal.builder()
                .lineNumber(1)
                .totalLines(1)
                .invoice(invoice)
                .title("SubTotal")
                .order(1)
                .totalAmountItbis1(Numbers.centesimal(posInvoice.getTotalItbis1()))
                .totalAmountItbis2(Numbers.centesimal(posInvoice.getTotalItbis3()))
                .totalAmountItbis3(Numbers.centesimal(posInvoice.getTotalItbisRate3()))
                .taxedTotalAmount(posInvoice.getTotalTaxedAmount())
                .itbisTotalAmount(posInvoice.getTotalItbisAmount())
                .totalItbisRate1(Numbers.centesimal(posInvoice.getTotalItbisRate1()))
                .totalItbisRate2(Numbers.centesimal(posInvoice.getTotalItbisRate2()))
                .totalItbisRate3(Numbers.centesimal(posInvoice.getTotalItbisRate3()))
                .totalAdditionalTaxes(posInvoice.getTotalAdditionalTaxes())
                .totalExcempt(posInvoice.getTotalExemptAmount())
                .subtotalAmount(posInvoice.getInvoiceTotalAmount())
                .build();
        // PERSISTS SUBTOTAL
        this.subtotalRepository.save(subtotal);
    }

    private void buildSubtotal(NoPosInvoiceHeader posInvoice, Invoice invoice) {
        // BUILD SUBTOTAL OBJECT
        final InvoiceSubtotal subtotal = InvoiceSubtotal.builder()
                .lineNumber(1)
                .totalLines(1)
                .invoice(invoice)
                .title("SubTotal")
                .order(1)
                .totalAmountItbis1(posInvoice.getTotalItbis1())
                .totalAmountItbis2(posInvoice.getTotalItbis3())
                .totalAmountItbis3(posInvoice.getTotalItbisRate3())
                .itbisTotalAmount(posInvoice.getTotalItbisAmount())
                .taxedTotalAmount(posInvoice.getTotalTaxedAmount())
                .totalItbisRate1(posInvoice.getTotalItbisRate1())
                .totalItbisRate2(posInvoice.getTotalItbisRate2())
                .totalItbisRate3(posInvoice.getTotalItbisRate3())
                .totalAdditionalTaxes(posInvoice.getTotalAdditionalTaxes())
                .totalExcempt(posInvoice.getTotalExemptAmount())
                .subtotalAmount(posInvoice.getInvoiceTotalAmount())
                .build();
        // PERSISTS SUBTOTAL
        this.subtotalRepository.save(subtotal);
    }

    private void buildPagination(PosInvoiceHeader posInvoice, Invoice invoice, List<InvoiceItem> items) {
        // BUILD PAGINATION OBJECT
        final InvoicePagination pagination = InvoicePagination.builder()
                .pageNumber(1)
                .startLineNum(1)
                .endLineNum(items.size())
                // I guess that should be the total amount, due that we only have one page
                .subtotalAmountItbis1(Numbers.centesimal(posInvoice.getTotalItbis1()))
                .subtotalAmountItbis2(Numbers.centesimal(posInvoice.getTotalItbis2()))
                .subtotalAmountItbis3(Numbers.centesimal(posInvoice.getTotalItbis3()))
                .subtotalTaxedAmount(posInvoice.getTotalTaxedAmount())
                .subtotalExempt(posInvoice.getTotalExemptAmount())
                .subtotalItbis(invoice.getTotalItbisAmount())
                .subtotalItbis1(Numbers.centesimal(posInvoice.getTotalItbis1()))
                .subtotalItbis2(Numbers.centesimal(posInvoice.getTotalItbis2()))
                .subtotalItbis3(Numbers.centesimal(posInvoice.getTotalItbis3()))
                .subtotalAdditionalTaxes(0.0)
                .subtotalIscSpecific(0.0)
                .subtotalOtherTaxes(0.0)
                .subtotalNotBillable(posInvoice.getNotBillableAmount())
                .grandSubtotalPage(posInvoice.getInvoiceTotalAmount())
                .invoice(invoice)
                .build();
        // PERSISTS PAGINATION
        this.paginationRepository.save(pagination);
        log.info(String.format(pagination_persisted, SystemSource.POS, invoice.getEncf()));
    }

    private void buildPagination(NoPosInvoiceHeader posInvoice, Invoice invoice, List<InvoiceItem> items) {
        // BUILD PAGINATION OBJECT
        final InvoicePagination pagination = InvoicePagination.builder()
                .pageNumber(1)
                .startLineNum(1)
                .endLineNum(items.size())
                // I guess that should be the total amount, due that we only have one page
                .subtotalAmountItbis1(posInvoice.getTotalItbis1())
                .subtotalAmountItbis2(posInvoice.getTotalItbis2())
                .subtotalAmountItbis3(posInvoice.getTotalItbis3())
                .subtotalTaxedAmount(posInvoice.getTotalTaxedAmount())
                .subtotalExempt(posInvoice.getTotalExemptAmount())
                .subtotalItbis(invoice.getTotalItbisAmount())
                .subtotalItbis1(posInvoice.getTotalItbis1())
                .subtotalItbis2(posInvoice.getTotalItbis2())
                .subtotalItbis3(posInvoice.getTotalItbis3())
                .subtotalAdditionalTaxes(0.0)
                .subtotalIscSpecific(0.0)
                .subtotalOtherTaxes(0.0)
                .subtotalNotBillable(posInvoice.getNotBillableAmount())
                .grandSubtotalPage(posInvoice.getInvoiceTotalAmount())
                .invoice(invoice)
                .build();
        // PERSISTS PAGINATION
        this.paginationRepository.save(pagination);
        log.info(String.format(pagination_persisted, SystemSource.NO_POS, invoice.getEncf()));
    }

    private void buildInvoiceBasics(Invoice invoice, List<InvoiceItem> items, List<InvoicePayment> payments) {
        // VALIDATES IF INVOICE EXISTS, IN THAT CASE, SHOULD REPLACE THE STORED VALUES.
        this.invoiceRepository.findTopByEncfAndEmitterRnc(invoice.getEncf(), invoice.getEmitterRnc())
                .ifPresent((existing) -> invoice.setId(existing.getId()));
        // SAVE INVOICE BEFORE ASSIGNING ALL OTHERS FIELDS
        this.invoiceRepository.save(invoice);
        // ITERATES OVER ITEMS, IN ORDER TO SAVE EACH WHILE SETTING INVOICE
        for (InvoiceItem item : items) {
            // REFERENCES NEWLY CREATED INVOICE
            item.setInvoice(invoice);
            // SAVE INVOICE ITEM
            this.invoiceItemRepository.save(item);
        }
        // ITERATES OVER PAYMENTS, IN ORDER TO SAVE EACH WHILE SETTING INVOICE
        for (InvoicePayment payment : payments) {
            // REFERENCES NEWLY CREATED INVOICE
            payment.setInvoice(invoice);
            // SAVE INVOICE PAYMENT
            this.paymentRepository.save(payment);
        }
    }
}
