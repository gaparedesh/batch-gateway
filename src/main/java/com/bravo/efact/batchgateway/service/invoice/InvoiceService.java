package com.bravo.efact.batchgateway.service.invoice;

import com.bravo.efact.batchgateway.domain.entity.nopos.NoPosInvoiceHeader;
import com.bravo.efact.batchgateway.domain.entity.pos.PosInvoiceHeader;
import com.bravo.efact.lib.domain.entity.DocumentType;
import com.bravo.efact.lib.domain.entity.IncomeType;
import com.bravo.efact.lib.domain.entity.Invoice;
import com.bravo.efact.lib.domain.entity.InvoiceItem;
import com.bravo.efact.lib.domain.entity.InvoiceLoadError;
import com.bravo.efact.lib.domain.entity.InvoicePayment;
import com.bravo.efact.lib.domain.entity.InvoiceStatus;
import com.bravo.efact.lib.domain.entity.InvoiceType;
import com.bravo.efact.lib.domain.entity.NcfChangeReason;
import com.bravo.efact.lib.domain.entity.PaymentCondition;
import com.bravo.efact.lib.domain.entity.TaxCategory;
import com.bravo.efact.lib.domain.entity.UnitMeasure;

import java.util.List;

public interface InvoiceService {

    String document_type = "Enviado";
    String invoice_status = "Pendiente";
    String invoice_persisted = "Info for invoice %s-%s, loaded correctly";
    String invoice_not_persisted = "Info for invoice %s-%s, could not be loaded";
    String invoice_total_loaded = "Invoice loaded: %d invoice loaded correctly";
    String pagination_persisted = "Pagination Info for invoice %s-%s, loaded correctly";

    Invoice save(PosInvoiceHeader posInvoice, Invoice invoice, List<InvoiceItem> items, List<InvoicePayment> payments);

    Invoice save(NoPosInvoiceHeader posInvoice, Invoice invoice, List<InvoiceItem> items, List<InvoicePayment> payments);

    List<InvoiceType> invoiceTypes();

    DocumentType documentType();

    InvoiceStatus invoiceStatus();

    List<IncomeType> incomeTypes();

    List<NcfChangeReason> changeReasons();

    List<PaymentCondition> paymentConditions();

    List<UnitMeasure> unitMeasures();

    List<TaxCategory> taxCategories();

    void invoiceLoadError(InvoiceLoadError error, String systemSource);

}
