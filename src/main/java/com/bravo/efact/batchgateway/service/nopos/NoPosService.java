package com.bravo.efact.batchgateway.service.nopos;

public interface NoPosService {
    void loadAndStoreData();
}
