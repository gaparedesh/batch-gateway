package com.bravo.efact.batchgateway.service.log;

import com.bravo.utils.domain.model.log.LogData;
import com.bravo.utils.domain.model.log.LogError;
import com.bravo.utils.domain.model.log.LogRequest;
import com.bravo.utils.domain.model.log.LogResponse;
import com.bravo.utils.domain.model.log.TransactionLog;
import com.bravo.utils.domain.util.BravoUtils;
import com.bravo.utils.service.log.LogService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class DefaultInternalLogService implements InternalLogService {
    private final LogService logService;

    public DefaultInternalLogService(LogService logService) {
        this.logService = logService;
    }


    @Override
    public void error(TransactionLog transaction, String message, String extra, Throwable error, Map<String, Object> params) {
        try {
            final String id = Optional.ofNullable(MDC.get(LogService.transaction_id))
                    .orElseGet(InternalLogService::init);
            final LogRequest request = LogRequest.builder()
                    .body(params)
                    .build();
            final LogResponse response = LogResponse
                    .builder()
                    .succeed(false)
                    .error(message)
                    .data(LogError.of(error))
                    .build();
            final LogData logData = LogData.builder()
                    .id(id)
                    .extra(extra)
                    .date(new Date())
                    .transaction(transaction)
                    .request(request)
                    .response(response)
                    .user(LogService.anonymous_user)
                    .type(LogData.LogType.THROWING)
                    .build();
            final String consoleData = BravoUtils.stringify(logData);
            log.error(consoleData);
            this.logService.log(logData);
        } catch (IllegalArgumentException e) {
            log.error("Error logging to elastic: {}", e.getMessage(), e);
            log.error("Error logging error: {}", error.getMessage(), error);
        }
    }
}
