package com.bravo.efact.batchgateway.service.pos;

public interface PosService {
    String RET_IND = "R";

    void loadAndStoreData();
}
