package com.bravo.efact.batchgateway.service.system;

import com.bravo.efact.batchgateway.domain.entity.efact.SystemDataLoaded;
import com.bravo.efact.lib.domain.model.SystemSource;

import java.util.Optional;

public interface SystemService {
    Optional<SystemDataLoaded> lastLoaded(SystemSource source);

    SystemDataLoaded setLastLoaded(SystemDataLoaded loaded);
}
