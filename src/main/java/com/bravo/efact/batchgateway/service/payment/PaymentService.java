package com.bravo.efact.batchgateway.service.payment;

import com.bravo.efact.lib.domain.entity.CurrencyType;
import com.bravo.efact.lib.domain.entity.PaymentType;

import java.util.List;

public interface PaymentService {
    List<PaymentType> paymentTypes();

    List<CurrencyType> currencyTypes();
}
