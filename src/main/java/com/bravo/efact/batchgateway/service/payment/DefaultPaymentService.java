package com.bravo.efact.batchgateway.service.payment;

import com.bravo.efact.batchgateway.domain.repository.efact.CurrencyTypeRepository;
import com.bravo.efact.batchgateway.domain.repository.efact.PaymentTypeRepository;
import com.bravo.efact.lib.domain.entity.CurrencyType;
import com.bravo.efact.lib.domain.entity.PaymentType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultPaymentService implements PaymentService {

    private final PaymentTypeRepository paymentTypeRepository;
    private final CurrencyTypeRepository currencyTypeRepository;

    public DefaultPaymentService(PaymentTypeRepository paymentTypeRepository, CurrencyTypeRepository currencyTypeRepository) {
        this.paymentTypeRepository = paymentTypeRepository;
        this.currencyTypeRepository = currencyTypeRepository;
    }

    @Override
    public List<PaymentType> paymentTypes() {
        return this.paymentTypeRepository.findAll();
    }

    @Override
    public List<CurrencyType> currencyTypes() {
        return this.currencyTypeRepository.findAll();
    }
}
