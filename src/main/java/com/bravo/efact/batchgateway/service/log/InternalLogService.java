package com.bravo.efact.batchgateway.service.log;

import com.bravo.utils.domain.model.log.TransactionLog;
import com.bravo.utils.service.log.LogService;
import org.slf4j.MDC;

import java.util.Map;
import java.util.UUID;

public interface InternalLogService {
    static String init() {
        final String id = UUID.randomUUID().toString();
        MDC.put(LogService.transaction_id, id);
        return id;
    }

    static void finish() {
        MDC.remove(LogService.transaction_id);
    }

    void error(TransactionLog transaction, String message, String extra, Throwable error, Map<String, Object> params);
}
