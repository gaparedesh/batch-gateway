package com.bravo.efact.batchgateway.service.nopos;

import com.bravo.efact.batchgateway.domain.entity.efact.SystemDataLoaded;
import com.bravo.efact.batchgateway.domain.entity.nopos.NoPosInvoiceDetail;
import com.bravo.efact.batchgateway.domain.entity.nopos.NoPosInvoiceHeader;
import com.bravo.efact.batchgateway.domain.entity.nopos.NoPosInvoiceReference;
import com.bravo.efact.batchgateway.domain.repository.nopos.NoPosInvoiceDetailRepository;
import com.bravo.efact.batchgateway.domain.repository.nopos.NoPosInvoiceHeaderRepository;
import com.bravo.efact.batchgateway.domain.repository.nopos.NoPosInvoiceRefRepository;
import com.bravo.efact.batchgateway.domain.repository.nopos.NoPosPaymentRepository;
import com.bravo.efact.batchgateway.service.invoice.InvoiceService;
import com.bravo.efact.batchgateway.service.payment.PaymentService;
import com.bravo.efact.batchgateway.service.system.SystemService;
import com.bravo.efact.batchgateway.util.DateFormatter;
import com.bravo.efact.batchgateway.util.Dates;
import com.bravo.efact.batchgateway.util.ExceptionUtils;
import com.bravo.efact.lib.domain.entity.CurrencyType;
import com.bravo.efact.lib.domain.entity.DocumentType;
import com.bravo.efact.lib.domain.entity.IncomeType;
import com.bravo.efact.lib.domain.entity.Invoice;
import com.bravo.efact.lib.domain.entity.InvoiceItem;
import com.bravo.efact.lib.domain.entity.InvoiceLoadError;
import com.bravo.efact.lib.domain.entity.InvoiceLoadErrorStatus;
import com.bravo.efact.lib.domain.entity.InvoicePayment;
import com.bravo.efact.lib.domain.entity.InvoiceStatus;
import com.bravo.efact.lib.domain.entity.InvoiceType;
import com.bravo.efact.lib.domain.entity.NcfChangeReason;
import com.bravo.efact.lib.domain.entity.PaymentCondition;
import com.bravo.efact.lib.domain.entity.PaymentType;
import com.bravo.efact.lib.domain.entity.TaxCategory;
import com.bravo.efact.lib.domain.entity.UnitMeasure;
import com.bravo.efact.lib.domain.model.CurrencyTypeEnum;
import com.bravo.efact.lib.domain.model.SystemSource;
import com.bravo.efact.lib.utils.Numbers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DefaultNoPosService implements NoPosService {

    private static final String  STATUS_LOAD_ERROR_NAME = "Pendiente";
    private static final String  STATUS_LOAD_ERROR_CODE = "01";
    private static final Long  STATUS_LOAD_ERROR_ID = 1L;


    private final String DEBITNOTE = "E33";
    private final String CREDITNOTE = "E34";

    @Value("${bravo.upload.nopos-invoice-top}")
    private int limit;

    private final SystemService systemService;
    private final PaymentService paymentService;
    private final InvoiceService invoiceService;
    private final NoPosInvoiceHeaderRepository headerRepository;
    private final NoPosInvoiceDetailRepository invoiceDetailRepository;
    private final NoPosPaymentRepository paymentRepository;
    private final NoPosInvoiceRefRepository noPosInvoiceRefRepository;

    public DefaultNoPosService(SystemService systemService,
                               PaymentService paymentService,
                               InvoiceService invoiceService,
                               NoPosInvoiceHeaderRepository headerRepository,
                               NoPosPaymentRepository paymentRepository,
                               NoPosInvoiceDetailRepository invoiceDetailRepository, NoPosInvoiceRefRepository noPosInvoiceRefRepository) {
        this.systemService = systemService;
        this.headerRepository = headerRepository;
        this.invoiceService = invoiceService;
        this.paymentService = paymentService;
        this.paymentRepository = paymentRepository;
        this.invoiceDetailRepository = invoiceDetailRepository;
        this.noPosInvoiceRefRepository = noPosInvoiceRefRepository;
    }

    @Override
    public void loadAndStoreData() {
        final SystemDataLoaded loaded = this.systemService.lastLoaded(SystemSource.NO_POS)
                .map(data -> {
                    final Page<NoPosInvoiceHeader> noPosInvoices = this.headerRepository.findAllByIdGreaterThanOrderByIdAsc(data.getLastDocument(), PageRequest.of(0, limit));
                    final List<Invoice> invoices = this.generateInvoices(noPosInvoices.toList());
                    return invoices.isEmpty() ? data : this.getDataLoaded(noPosInvoices.toList());
                }).orElseGet(() -> {
                    final List<NoPosInvoiceHeader> noPosInvoices = this.headerRepository.findAllByOrderById();
                    SystemDataLoaded dataLoaded = SystemDataLoaded.builder()
                            .loadedTmp(new Date())
                            .firstDocument(0L)
                            .lastDocument(0L)
                            .totalLoaded(0)
                            .systemSource(SystemSource.NO_POS)
                            .build();

                    this.generateInvoices(noPosInvoices);

                    return noPosInvoices.isEmpty() ? dataLoaded : this.getDataLoaded(noPosInvoices);
                });
        this.systemService.setLastLoaded(loaded);
    }

    private List<Invoice> generateInvoices(List<NoPosInvoiceHeader> noPosInvoices) {
        final List<CurrencyType> currencyTypes = this.paymentService.currencyTypes();
        final List<PaymentType> paymentTypes = this.paymentService.paymentTypes();

        final List<IncomeType> incomeTypes = this.invoiceService.incomeTypes();
        final List<PaymentCondition> conditions = this.invoiceService.paymentConditions();
        final List<TaxCategory> taxCategories = this.invoiceService.taxCategories();
        final DocumentType documentType = this.invoiceService.documentType();
        final InvoiceStatus invoiceStatus = this.invoiceService.invoiceStatus();
        final List<InvoiceType> invoiceTypes = this.invoiceService.invoiceTypes();
        final List<NcfChangeReason> changeReasons = this.invoiceService.changeReasons();
        final List<UnitMeasure> measures = this.invoiceService.unitMeasures();

        final List<Invoice> stored = noPosInvoices.stream().map(noPosInvoice -> {
            final List<InvoicePayment> payments = this.paymentRepository
                    .findAllByEcfEquals(noPosInvoice.getEcf()).stream().map(p -> {
                        final PaymentType paymentType = paymentTypes.stream()
                                .filter(t -> Numbers.equals(Integer.toString(t.getCode()), p.getPaymentType()))
                                .findFirst().orElse(null);
                        return new InvoicePayment()
                                .setPaymentType(paymentType)
                                .setAmount(p.getAmount());
                    }).collect(Collectors.toList());
            // Returns
            final InvoiceType invoiceType = invoiceTypes.stream()
                    .filter(t -> t.getCode().equals(noPosInvoice.getType()))
                    .findFirst().orElse(null);
            final PaymentCondition paymentCondition = conditions.stream()
                    .filter(t -> t.getCode().equals(noPosInvoice.getPaymentCondition()))
                    .findFirst().orElse(null);
            final CurrencyType currencyType = currencyTypes.stream()
                    .filter(t -> t.getIsoCode().equals(noPosInvoice.getCurrencyType()))
                    .findFirst().orElse(null);
            final NcfChangeReason changeReason = changeReasons.stream()
                    .filter(t -> t.getCode().equals(-1))
                    .findFirst().orElse(null);
            final IncomeType incomeType = incomeTypes.stream()
                    .filter(t -> t.getCode().equals(noPosInvoice.getIncomeType()))
                    .findFirst().orElse(null);
            List<InvoiceItem> items = this.generateInvoiceItems(noPosInvoice.getEcf(), measures, taxCategories);

            // Getting ncf modified for Credit Note or Debit Node
            if(noPosInvoice.getEcf().startsWith(DEBITNOTE) || noPosInvoice.getEcf().startsWith(CREDITNOTE)) {
                Optional<NoPosInvoiceReference> reference = this.noPosInvoiceRefRepository.findByEncf(noPosInvoice.getEcf());
                if (!reference.isPresent() || Objects.isNull(reference.get().getEncfModified())) {
                    log.error("NCF MODIFIED NOT FOUND FOR ENCF: "+noPosInvoice.getEcf());
                }else {
                    noPosInvoice.setEncfModified(reference.get().getEncfModified());
                }
            }
            try {
                final Invoice invoice = this.buildInvoice(
                        documentType,
                        invoiceStatus,
                        noPosInvoice,
                        payments,
                        invoiceType,
                        currencyType,
                        incomeType,
                        changeReason,
                        paymentCondition
                );
                return this.invoiceService.save(noPosInvoice, invoice, items, payments);
            } catch (Exception ex) {
                loadInvoiceError(noPosInvoice, ex, noPosInvoices);
                log.error("ERROR CARGANDO COMPROBANTE "+noPosInvoice.getEcf());
                log.error(ex.getMessage(), ex);
            }

            return null;
        }).filter(Objects::nonNull).collect(Collectors.toList());
        log.info(String.format(InvoiceService.invoice_total_loaded, stored.size()));
        return stored;
    }

    private void loadInvoiceError(NoPosInvoiceHeader noPosInvoice, Exception ex, List<NoPosInvoiceHeader> noPosInvoices) {

       try{
           InvoiceLoadErrorStatus status = InvoiceLoadErrorStatus.builder().id(STATUS_LOAD_ERROR_ID)
                   .code(STATUS_LOAD_ERROR_CODE).name(STATUS_LOAD_ERROR_NAME).build();

           InvoiceLoadError invoiceLoadError = InvoiceLoadError.builder()
                   .encf(noPosInvoice.getEcf())
                   .internalId(noPosInvoice.getId())
                   .emitterRnc(noPosInvoice.getEmitterRnc())
                   .buyerRnc(noPosInvoice.getBuyerRnc())
                   .status(status)
                   .systemSource(SystemSource.NO_POS.name())
                   .stacktrace(ExceptionUtils.exceptionToString(ex))
                   .createdDate(new Date()).build();


           invoiceService.invoiceLoadError(invoiceLoadError, SystemSource.NO_POS.name());

           this.systemService.setLastLoaded(this.getDataLoaded(noPosInvoices));

       }catch (Exception e){
           log.error("ERROR CARGANDO COMPROBANTE {} NO SE PUDO CARGAR EN LA TABLA DE LOG INVOICELOADERROR", noPosInvoice.getEcf(), e);
       }

    }

    private Invoice buildInvoice(DocumentType documentType, InvoiceStatus invoiceStatus, NoPosInvoiceHeader noPos, List<InvoicePayment> payments, InvoiceType invoiceType, CurrencyType currencyType, IncomeType incomeType, NcfChangeReason changeReason, PaymentCondition paymentCondition) {
        final boolean dop = CurrencyTypeEnum.of(currencyType).isDop();
        final DateFormatter formatter = DateFormatter.LIBRA;
        double totalTaxedAmountOther = Objects.isNull(noPos.getTotalTaxedAmountOther()) ? 0 : noPos.getTotalTaxedAmountOther();
        return Invoice.builder()
                .version(1.0)
                .systemSource(SystemSource.NO_POS)
                .encf(noPos.getEcf())
                .modifiedEfc(noPos.getEncfModified())
                .sequenceExpDate(formatter.format(noPos.getSequenceExpirationDate(), Dates.nextYearEnd()))
                .internalId(noPos.getLibraInvoiceNumber())
                .exchangeRate(Double.parseDouble(noPos.getExchangeRate()))
                .invoiceType(invoiceType)
                .creationDate(Objects.isNull(noPos.getEmissionDate())?null:noPos.getEmissionDate())
                .updateDate(new Date())
                .documentType(documentType)
                .invoiceStatus(invoiceStatus)
                .currencyType(currencyType)
                .incomeType(incomeType)
                .emitterRnc(noPos.getEmitterRnc())
                .emitterBusinessName(noPos.getEmitterBusinessName())
                .emitterAddress(noPos.getEmitterAddress())
                .emissionDate(noPos.getEmissionDate())
                .buyerRnc(noPos.getBuyerRnc())
                .foreignDni(noPos.getForeingDni())
                .buyerBusinessName(noPos.getBuyerBusinessName())
                .totalItbisAmount(dop ? noPos.getTotalItbisAmount() : noPos.getTotalItbisAmountOther())
                .totalTaxedAmount(dop ? noPos.getTotalTaxedAmount() : totalTaxedAmountOther)
                .totalExemptAmount(dop ? noPos.getTotalExemptAmount() : noPos.getTotalExemptAmountOther())
                .invoiceTotalAmount(dop ? noPos.getInvoiceTotalAmount() : noPos.getInvoiceTotalAmountOther())
                .notBillableAmount(noPos.getNotBillableAmount())
                .paymentDeadline(Objects.isNull(noPos.getPaymentDeadline())?noPos.getEmissionDate(): formatter.format(noPos.getPaymentDeadline()))
                .totalPages(Numbers.integer(noPos.getTotalPages()))
                .creditNoteInd(noPos.getCreditNoteInd())
                .deferredShipping(noPos.getDeferredShippingInd())
                .taxedAmountInd(noPos.getTaxedAmountInd())
                .emitterEmail(noPos.getEmitterEmail())
                .totalAdditionalTaxes(dop ? noPos.getTotalAdditionalTaxes() : noPos.getTotalAdditionalTaxesOther())
                .totalDueAmount(noPos.getTotalDueAmount())
                .modifReason(changeReason)
                .paymentCondition(paymentCondition)
                .payments(payments)
                .commercialOrganization(noPos.getCommercialOrganization())
                .build();

    }

    private List<InvoiceItem> generateInvoiceItems(String ecf, List<UnitMeasure> measures, List<TaxCategory> taxCategories) {
        final List<NoPosInvoiceDetail> details = this.invoiceDetailRepository.findAllByEcfEquals(ecf);
        final List<InvoiceItem> items = new ArrayList<>();
        for (int i = 0; i < details.size(); i++) {
            final NoPosInvoiceDetail detail = details.get(i);
            final UnitMeasure unitMeasure = measures.stream()
                    .filter(m -> m.is(detail.getUnitMeasure()))
                    .findFirst().orElse(null);
            final TaxCategory taxCategory = taxCategories.stream()
                    .filter(m -> m.getCode().equals(detail.getTaxCategory()))
                    .findFirst().orElse(null);
            final InvoiceItem item = InvoiceItem
                    .builder()
                    .lineNumber(detail.getLineNumber())
                    .lineNumber(i + 1)
                    .itemDescription(detail.getItemDescription())
                    .itemQuantity(detail.getItemQuantity())
                    .referenceQuantity(detail.getReferenceQuantity())
                    .unitMeasure(unitMeasure)
                    .creationDate(new Date())
                    .refUnitMeasure(unitMeasure)
                    .refUnitPrice(detail.getRefUnitPrice())
                    .unitPriceItem(detail.getUnitPriceItem())
                    .discountAmount(detail.getDiscountAmount())
                    .itemAmount(detail.getItemAmount())
                    .taxCategory(taxCategory)
                    .retentionAgentInd(detail.getRetentionAgentIndicator())
                    .isrRetAmount(detail.getRetentionISRRetenido())
                    .itbisRetAmount(detail.getRetentionITBISAmount())
                    .itbis(0.0)
                    .serviceInd(detail.getServiceIndicator())
                    .alcoholDegrees(0.0)
                    .surchargeAmount(detail.getSurcharge())
                    .build();
            items.add(item);
        }
        return items;
    }

    private SystemDataLoaded getDataLoaded(List<NoPosInvoiceHeader> invoices) {
        final int size = invoices.size();
        return SystemDataLoaded.builder()
                .firstDocument(invoices.get(0).getId())
                .lastDocument(invoices.get(size - 1).getId())
                .totalLoaded(size)
                .loadedTmp(new Date())
                .systemSource(SystemSource.NO_POS)
                .build();
    }
}
