package com.bravo.efact.batchgateway.service.pos;

import com.bravo.efact.batchgateway.domain.entity.efact.SystemDataLoaded;
import com.bravo.efact.batchgateway.domain.entity.pos.PosInvoiceDetail;
import com.bravo.efact.batchgateway.domain.entity.pos.PosInvoiceHeader;
import com.bravo.efact.batchgateway.domain.repository.pos.PosInvoiceDetailRepository;
import com.bravo.efact.batchgateway.domain.repository.pos.PosInvoiceHeaderRepository;
import com.bravo.efact.batchgateway.domain.repository.pos.PosPaymentRepository;
import com.bravo.efact.batchgateway.service.invoice.InvoiceService;
import com.bravo.efact.batchgateway.service.payment.PaymentService;
import com.bravo.efact.batchgateway.service.system.SystemService;
import com.bravo.efact.batchgateway.util.ExceptionUtils;
import com.bravo.efact.lib.domain.entity.CurrencyType;
import com.bravo.efact.lib.domain.entity.DocumentType;
import com.bravo.efact.lib.domain.entity.IncomeType;
import com.bravo.efact.lib.domain.entity.Invoice;
import com.bravo.efact.lib.domain.entity.InvoiceItem;
import com.bravo.efact.lib.domain.entity.InvoiceLoadError;
import com.bravo.efact.lib.domain.entity.InvoiceLoadErrorStatus;
import com.bravo.efact.lib.domain.entity.InvoicePayment;
import com.bravo.efact.lib.domain.entity.InvoiceStatus;
import com.bravo.efact.lib.domain.entity.InvoiceType;
import com.bravo.efact.lib.domain.entity.NcfChangeReason;
import com.bravo.efact.lib.domain.entity.PaymentCondition;
import com.bravo.efact.lib.domain.entity.PaymentType;
import com.bravo.efact.lib.domain.entity.TaxCategory;
import com.bravo.efact.lib.domain.entity.UnitMeasure;
import com.bravo.efact.lib.domain.model.SystemSource;
import com.bravo.efact.lib.utils.Numbers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DefaultPosService implements PosService {

    private static final String  STATUS_LOAD_ERROR_NAME = "Pendiente";
    private static final String  STATUS_LOAD_ERROR_CODE = "01";
    private static final Long  STATUS_LOAD_ERROR_ID = 1L;

    @Value("${bravo.upload.invoice-top}")
    private int limit;

    private final SystemService systemService;
    private final PaymentService paymentService;
    private final InvoiceService invoiceService;
    private final PosInvoiceHeaderRepository headerRepository;
    private final PosInvoiceDetailRepository invoiceDetailRepository;
    private final PosPaymentRepository posPaymentRepository;

    public DefaultPosService(SystemService systemService,
                             PaymentService paymentService,
                             InvoiceService invoiceService,
                             PosInvoiceHeaderRepository headerRepository,
                             PosPaymentRepository posPaymentRepository,
                             PosInvoiceDetailRepository invoiceDetailRepository) {
        this.systemService = systemService;
        this.headerRepository = headerRepository;
        this.invoiceService = invoiceService;
        this.paymentService = paymentService;
        this.posPaymentRepository = posPaymentRepository;
        this.invoiceDetailRepository = invoiceDetailRepository;
    }

    @Override
    public void loadAndStoreData() {
        final SystemDataLoaded loaded = this.systemService.lastLoaded(SystemSource.POS)
                .map(data -> {
                    final Page<PosInvoiceHeader> posInvoices = this.headerRepository.
                            findAllByIdGreaterThanOrderByIdAsc(data.getLastDocument(), PageRequest.of(0,limit));
                    final List<Invoice> invoices = this.generateInvoice(posInvoices.toList());
                    return invoices.isEmpty() ? data : this.getDataLoaded(posInvoices.toList());
                }).orElseGet(() -> {
                    final List<PosInvoiceHeader> posInvoices = this.headerRepository.findAllByOrderById();
                    this.generateInvoice(posInvoices);
                    return posInvoices.isEmpty() ? SystemDataLoaded.builder()
                            .loadedTmp(new Date())
                            .firstDocument(0L)
                            .lastDocument(0L)
                            .totalLoaded(0)
                            .systemSource(SystemSource.POS)
                            .build() :
                            this.getDataLoaded(posInvoices);
                });
        this.systemService.setLastLoaded(loaded);
    }

    private List<Invoice> generateInvoice(List<PosInvoiceHeader> posInvoices) {
        final List<PaymentType> paymentTypes = this.paymentService.paymentTypes();
        final List<CurrencyType> currencyTypes = this.paymentService.currencyTypes();

        final DocumentType documentType = this.invoiceService.documentType();
        final List<IncomeType> incomeTypes = this.invoiceService.incomeTypes();
        final InvoiceStatus invoiceStatus = this.invoiceService.invoiceStatus();
        final List<InvoiceType> invoiceTypes = this.invoiceService.invoiceTypes();
        final List<NcfChangeReason> changeReasons = this.invoiceService.changeReasons();
        final List<PaymentCondition> conditions = this.invoiceService.paymentConditions();
        final List<UnitMeasure> measures = this.invoiceService.unitMeasures();
        final List<TaxCategory> taxCategories = this.invoiceService.taxCategories();

        final List<Invoice> stored = posInvoices.stream().map(posInvoice -> {
            final List<InvoicePayment> payments = this.posPaymentRepository
                    .findAllByIdEquals(posInvoice.getId()).stream().map(p -> {
                        final PaymentType paymentType = paymentTypes.stream()
                                .filter(t -> Numbers.equals(Integer.toString(t.getCode()), p.getPaymentType()))
                                .findFirst().orElse(null);
                        return new InvoicePayment()
                                .setPaymentType(paymentType)
                                .setAmount(p.getAmount());
                    }).collect(Collectors.toList());
            // Returns
            final InvoiceType invoiceType = invoiceTypes.stream()
                    .filter(t -> t.getCode().equals(posInvoice.getType()))
                    .findFirst().orElse(null);
            final CurrencyType currencyType = currencyTypes.stream()
                    .filter(t -> t.getIsoCode().equals("DOP"))
                    .findFirst().orElse(null);
            final IncomeType incomeType = incomeTypes.stream()
                    .filter(t -> t.getCode().equals(posInvoice.getIncomeType()))
                    .findFirst().orElse(null);
            final NcfChangeReason changeReason = changeReasons.stream()
                    .filter(t -> t.getCode().equals(-1))
                    .findFirst().orElse(null);
            final PaymentCondition paymentCondition = conditions.stream()
                    .filter(t -> t.getCode().equals(posInvoice.getPaymentCondition()))
                    .findFirst().orElse(null);
            List<InvoiceItem> items = this.generateInvoiceItems(posInvoice.getId(), measures, taxCategories);

            try {
                final Invoice invoice = this.buildInvoice(
                        documentType,
                        invoiceStatus,
                        posInvoice,
                        invoiceType,
                        currencyType,
                        incomeType,
                        changeReason,
                        paymentCondition
                );
                return this.invoiceService.save(posInvoice, invoice, items, payments);
            } catch (Exception ex) {
                loadInvoiceError(posInvoice, ex, posInvoices);
                log.error("ERROR CARGANDO COMPROBANTE " + posInvoice.getEcf());
                log.error(ex.getMessage(), ex);
            }

            return null;
        }).filter(Objects::nonNull).collect(Collectors.toList());
        log.info(String.format(InvoiceService.invoice_total_loaded, stored.size()));
        return stored;
    }

    private void loadInvoiceError(PosInvoiceHeader posInvoice, Exception ex, List<PosInvoiceHeader> posInvoices) {
        try{
            InvoiceLoadErrorStatus status = InvoiceLoadErrorStatus.builder()
                    .id(STATUS_LOAD_ERROR_ID)
                    .code(STATUS_LOAD_ERROR_CODE)
                    .name(STATUS_LOAD_ERROR_NAME)
                    .build();

            InvoiceLoadError invoiceLoadError = InvoiceLoadError.builder()
                    .encf(posInvoice.getEcf())
                    .internalId(posInvoice.getId())
                    .emitterRnc(posInvoice.getEmitterRnc())
                    .buyerRnc(posInvoice.getBuyerRnc())
                    .status(status)
                    .systemSource(SystemSource.POS.name())
                    .stacktrace(ExceptionUtils.exceptionToString(ex))
                    .createdDate(new Date())
                    .build();

            invoiceService.invoiceLoadError(invoiceLoadError, SystemSource.POS.name());

            this.systemService.setLastLoaded(this.getDataLoaded(posInvoices));
        }catch (Exception e){
            log.error("ERROR CARGANDO COMPROBANTE {} NO SE PUDO CARGAR EN LA TABLA DE LOG INVOICELOADERROR", posInvoice.getEcf(), e);
        }
    }

    private Invoice buildInvoice(DocumentType documentType,
                                 InvoiceStatus invoiceStatus,
                                 PosInvoiceHeader posInvoice,
                                 InvoiceType invoiceType,
                                 CurrencyType currencyType,
                                 IncomeType incomeType,
                                 NcfChangeReason changeReason,
                                 PaymentCondition paymentCondition) {
        return Invoice.builder()
                .version(1.0)
                .systemSource(SystemSource.POS)
                .encf(posInvoice.getEcf())
                .modifiedEfc(posInvoice.getModifiedEcf())
                .sequenceExpDate(posInvoice.getSequenceExpirationDate())
                .internalId(String.valueOf(posInvoice.getId()))
                .exchangeRate(0D)
                .invoiceType(invoiceType)
                .creationDate(posInvoice.getPurchaseOrderDate())
                .updateDate(new Date())
                .documentType(documentType)
                .invoiceStatus(invoiceStatus)
                .currencyType(currencyType)
                .incomeType(incomeType)
                .emitterRnc(posInvoice.getEmitterRnc())
                .emitterBusinessName(posInvoice.getEmitterBusinessName())
                .emitterAddress(posInvoice.getEmitterAddress())
                .emissionDate(posInvoice.getPurchaseOrderDate())
                .buyerRnc(posInvoice.getBuyerRnc())
                .foreignDni(posInvoice.getForeignDni())
                .buyerBusinessName(posInvoice.getBuyerBusinessName())
                .totalItbisAmount(posInvoice.getTotalItbisAmount())
                .totalTaxedAmount(posInvoice.getTotalTaxedAmount())
                .totalExemptAmount(posInvoice.getTotalExemptAmount())
                .invoiceTotalAmount(posInvoice.getInvoiceTotalAmount())
                .notBillableAmount(posInvoice.getNotBillableAmount())
                .paymentDeadline(posInvoice.getPaymentDeadline())
                .totalPages(posInvoice.getTotalPages())
                .creditNoteInd(posInvoice.getCreditNoteInd())
                .deferredShipping(posInvoice.getDeferredShippingInd())
                .taxedAmountInd(posInvoice.getTaxedAmountInd())
                .emitterPhone(posInvoice.getEmitterPhone())
                .emitterEmail(posInvoice.getEmitterMail())
                .totalAdditionalTaxes(posInvoice.getTotalAdditionalTaxes())
                .totalDueAmount(posInvoice.getTotalDueAmount())
                .modifReason(changeReason)
                .paymentCondition(paymentCondition)
                .build();
    }

    private List<InvoiceItem> generateInvoiceItems(Long id, List<UnitMeasure> measures, List<TaxCategory> taxCategories) {
        final List<PosInvoiceDetail> details = this.invoiceDetailRepository.findAllByIdEquals(id);
        final List<InvoiceItem> items = new ArrayList<>();
        for (int i = 0; i < details.size(); i++) {
            final PosInvoiceDetail detail = details.get(i);
            final UnitMeasure unitMeasure = measures.stream()
                    .filter(m -> m.is(detail.getUnitMeasure()))
                    .findFirst().orElse(null);
            final TaxCategory taxCategory = taxCategories.stream()
                    .filter(m -> m.getCode().equals(detail.getTaxCategory()))
                    .findFirst().orElse(null);
            final InvoiceItem item = InvoiceItem
                    .builder()
                    .lineNumber(i + 1)
                    .itemDescription(detail.getItemDescription())
                    .itemQuantity(detail.getItemQuantity())
                    .itbis(detail.getItbis())
                    .referenceQuantity(0.0)
                    .unitMeasure(unitMeasure)
                    .creationDate(new Date())
                    .refUnitMeasure(unitMeasure)
                    .refUnitPrice(detail.getRefUnitPrice())
                    .unitPriceItem(detail.getUnitPriceItem())
                    .discountAmount(detail.getDiscountAmount())
                    .itemAmount(detail.getItemAmount())
                    .taxCategory(taxCategory)
                    .retentionAgentInd(RET_IND)
                    .isrRetAmount(0.0)
                    .itbisRetAmount(0.0)
                    .serviceInd(false)
                    .alcoholDegrees(0.0)
                    .surchargeAmount(0.0)
                    .build();
            items.add(item);
        }
        return items;
    }

    private SystemDataLoaded getDataLoaded(List<PosInvoiceHeader> invoices) {
        final int size = invoices.size();
        return SystemDataLoaded.builder()
                .firstDocument(invoices.get(0).getId())
                .lastDocument(invoices.get(size - 1).getId())
                .totalLoaded(size)
                .loadedTmp(new Date())
                .systemSource(SystemSource.POS)
                .build();
    }
}
