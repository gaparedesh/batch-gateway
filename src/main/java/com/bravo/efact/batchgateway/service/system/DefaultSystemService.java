package com.bravo.efact.batchgateway.service.system;

import com.bravo.efact.batchgateway.domain.entity.efact.SystemDataLoaded;
import com.bravo.efact.batchgateway.domain.repository.efact.SystemDataLoadedRepository;
import com.bravo.efact.lib.domain.model.SystemSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class DefaultSystemService implements SystemService {

    private final SystemDataLoadedRepository dataLoadedRepository;

    public DefaultSystemService(SystemDataLoadedRepository dataLoadedRepository) {
        this.dataLoadedRepository = dataLoadedRepository;
    }

    @Override
    public Optional<SystemDataLoaded> lastLoaded(SystemSource source) {
        return this.dataLoadedRepository.findTopBySystemSourceOrderByLastDocumentDesc(source);
    }

    @Override
    @Transactional
    public SystemDataLoaded setLastLoaded(SystemDataLoaded loaded) {
        return this.dataLoadedRepository.save(loaded);
    }
}
