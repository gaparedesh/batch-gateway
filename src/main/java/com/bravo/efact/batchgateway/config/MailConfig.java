package com.bravo.efact.batchgateway.config;

import com.bravo.utils.domain.model.mail.MailProperties;
import com.bravo.utils.service.mail.DefaultMailService;
import com.bravo.utils.service.mail.MailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MailConfig {

    @Value("${mail.senderAddress}")
    private String mail;
    @Value("${mail.senderName}")
    private String name;
    @Value("${mail.apiKey}")
    private String apiKey;

    @Bean
    public MailService mailService() {
        return new DefaultMailService(MailProperties.builder()
                .defaultSenderEmail(mail)
                .defaultSenderName(name)
                .sendGridApiKey(apiKey)
                .build());
    }
}
