package com.bravo.efact.batchgateway.config;

import com.bravo.utils.domain.model.log.ElasticConfig;
import com.bravo.utils.exception.MissingConfigException;
import com.bravo.utils.service.log.ElasticLogService;
import com.bravo.utils.service.log.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Slf4j
@Configuration
public class ElasticLogConfig {


    public ElasticConfig elasticConfig(String elasticUrl) {
        if (Objects.isNull(elasticUrl)) {
            throw new MissingConfigException("Missing configuration property {bravo.elastic.url}.");
        } else {
            log.warn("Using default ElasticConfig");
            return ElasticConfig.builder().defaultUser(LogService.anonymous_user).restTemplate(new RestTemplate()).resource(elasticUrl).build();
        }
    }

    @Bean
    public LogService logService(final HttpServletRequest request, @Value("${bravo.elastic.url}") String elasticUrl) {
        return new ElasticLogService(elasticConfig(elasticUrl), request);
    }
}
