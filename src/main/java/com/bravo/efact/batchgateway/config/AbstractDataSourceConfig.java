package com.bravo.efact.batchgateway.config;

import com.bravo.efact.lib.domain.model.SystemSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public abstract class AbstractDataSourceConfig {

    private final Environment environment;
    private final SystemSource source;

    protected AbstractDataSourceConfig(Environment environment, SystemSource source) {
        this.environment = environment;
        this.source = source;
    }

    protected LocalContainerEntityManagerFactoryBean getFactoryBean(LocalContainerEntityManagerFactoryBean factory) {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        factory.setJpaVendorAdapter(vendorAdapter);
        Map<String, Object> properties = new HashMap<>();
        // FUNCIONA Y YA
        properties.put("hibernate.hbm2ddl.auto", environment.getProperty("hibernate.hbm2ddl.auto"));
        properties.put("javax.persistence.validation.mode", environment.getProperty("spring.jpa.properties.javax.persistence.validation.mode"));
        properties.put("hibernate.dialect", environment.getProperty("hibernate.dialect"));
        factory.setJpaPropertyMap(properties);
        log.info(String.format("Loading database factory for datasource: %s", source));
        return factory;
    }
}
