package com.bravo.efact.batchgateway.config;

import com.bravo.efact.lib.domain.model.SystemSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        basePackages = "com.bravo.efact.batchgateway.domain.repository.nopos",
        entityManagerFactoryRef = "noposEntityManager",
        transactionManagerRef = "noposTransactionManager"
)
public class NoPOSPersistenceConfig extends AbstractDataSourceConfig {


    public NoPOSPersistenceConfig(Environment environment) {
        super(environment, SystemSource.NO_POS);
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.no-pos")
    public DataSource noPosDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean noposEntityManager() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(noPosDataSource());
        entityManagerFactoryBean.setPackagesToScan("com.bravo.efact.batchgateway.domain.entity.nopos");
        return super.getFactoryBean(entityManagerFactoryBean);
    }

    @Bean
    public PlatformTransactionManager noposTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(noposEntityManager().getObject());
        return transactionManager;
    }

}
