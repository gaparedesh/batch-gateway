package com.bravo.efact.batchgateway.config;

import com.bravo.efact.lib.domain.model.SystemSource;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        basePackages = "com.bravo.efact.batchgateway.domain.repository.efact",
        entityManagerFactoryRef = "efactEntityManager",
        transactionManagerRef = "efactTransactionManager"
)
public class EfactPersistenceConfig extends AbstractDataSourceConfig {

    final String[] managed_entities = {
            "com.bravo.efact.lib",
            "com.bravo.efact.batchgateway.domain.entity.efact"
    };

    public EfactPersistenceConfig(Environment environment) {
        super(environment, SystemSource.EFACT);
    }

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.efact")
    public DataSourceProperties efactDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.efact.hikari")
    public DataSource efactDataSource(){return efactDataSourceProperties().initializeDataSourceBuilder().build();}

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean efactEntityManager() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(efactDataSource());
        entityManagerFactoryBean.setPackagesToScan(managed_entities);
        return super.getFactoryBean(entityManagerFactoryBean);
    }

    @Bean
    @Primary
    public PlatformTransactionManager efactTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(efactEntityManager().getObject());
        return transactionManager;
    }
}
