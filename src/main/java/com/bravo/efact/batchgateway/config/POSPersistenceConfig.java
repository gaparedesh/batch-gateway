package com.bravo.efact.batchgateway.config;

import com.bravo.efact.lib.domain.model.SystemSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        basePackages = "com.bravo.efact.batchgateway.domain.repository.pos",
        entityManagerFactoryRef = "posEntityManager",
        transactionManagerRef = "posTransactionManager"
)
public class POSPersistenceConfig extends AbstractDataSourceConfig {

    public POSPersistenceConfig(Environment environment) {
        super(environment, SystemSource.POS);
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.pos")
    public DataSource posDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean posEntityManager() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(posDataSource());
        entityManagerFactoryBean.setPackagesToScan("com.bravo.efact.batchgateway.domain.entity.pos");
        return super.getFactoryBean(entityManagerFactoryBean);
    }

    @Bean
    public PlatformTransactionManager posTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(posEntityManager().getObject());
        return transactionManager;
    }
}
